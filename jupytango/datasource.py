# ===========================================================================
#  This file is part of the Tango Ecosystem
#
#  Copyright 2017-2020 Synchrotron SOLEIL, St.Aubin, France
#  Copyright 2021-EOT ESRF, Grenoble, France
#
#  This is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Lesser General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
#
#  This is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
#  more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with This.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================================

import numpy as np
import pandas as pd
from jupytango.tools import *  

# ------------------------------------------------------------------------------
module_logger = logging.getLogger(__name__)
module_logger.setLevel("ERROR")

# ------------------------------------------------------------------------------
class ChannelData(object):
    """channel data"""

    Format = enum(
        'SCALAR',
        'SPECTRUM',
        'IMAGE',
        'UNKNOWN'
    )
    
    def __init__(self, name='anonymous'):
        # name
        self._name = name
        # format
        self._format = ChannelData.Format.UNKNOWN
        # the data (np.arrays whatever is the data format)
        self._time = None # timestamps
        self._data = None # values
        # error flag (error could have occured but data can be valid)
        self._has_error = False
        # data flag 
        self._has_valid_data = False
        # error txt
        self._error = "no error"
        # exception caught
        self._exception = None

    @property
    def name(self):
        return self._name

    @property
    def format(self):
        return self._format

    @property
    def has_error(self):
        return self._has_error

    @property
    def error(self):
        return self._error

    @property
    def exception(self):
        return self._exception

    @property
    def has_valid_data(self):
        return self._has_valid_data
    
    @property
    def time(self):
        return self._time 

    @time.setter
    def time(self, t):
        raise Exception("use ChannelData.set_data to attach some data to a ChannelData")
        
    @property
    def data(self):
        return self._data
    
    @data.setter
    def data(self, d):
        raise Exception("use ChannelData.set_data to attach some data to a ChannelData")
        
    def set_data(self, time, data, format):
        assert(isinstance(time, np.ndarray))
        assert(isinstance(data, np.ndarray))
        self._time = time
        self._data = data
        self._format = format
        self._has_valid_data = True
                            
    def clear_data(self):
        self._time = time
        self._data = data
        self._format = ChannelData.UNKNOWN
        self._has_valid_data = False

    def set_error(self, err=None, exc=None):
        if not self._has_error:
            self._has_error = True
            self._error = "unknown error" if err is None else err
            self._exception = Exception("unknown error") if exc is None else exc

    def clear_error(self):
        self._has_error = False
        self._error = "no error"
        self._exception = None
        

# ------------------------------------------------------------------------------
class DataSource(object):
    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    def refresh_period_changed(self, up):
        pass
    
    def history_buffer_depth_changed(self, hbd):
        pass
    
    def pull_data(self):
        return ChannelData(self._name)

    def reset(self):
        pass
    
    def is_scalar(self):
        return False

    def is_spectrum(self):
        pass

    def is_image(self):
        pass

    def cleanup(self):
        pass