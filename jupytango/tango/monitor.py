# ===========================================================================
#  This file is part of the Tango Ecosystem
#
#  Copyright 2017-2020 Synchrotron SOLEIL, St.Aubin, France
#  Copyright 2021-EOT ESRF, Grenoble, France
#
#  This is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Lesser General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
#
#  This is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
#  more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with This.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================================

import time
import datetime
import logging
from collections import deque
import threading
import hashlib

import PyTango as tango

from bokeh.models import ColumnDataSource
from bokeh.models import CustomJS
from bokeh.models.mappers import LinearColorMapper
from bokeh.models.tools import HoverTool
from bokeh.palettes import Plasma256, Viridis256, Inferno256, Greys256
from bokeh.io import show, output_notebook, push_notebook, reset_output
from bokeh.plotting import figure
from bokeh.resources import INLINE

from jupytango.tools import silent_catch
from jupytango.plots import GenericChannel, ScalarChannel, SpectrumChannel, ImageChannel
from jupytango.plots import DataStream, DataStreamer, DataStreamerController
from jupytango.tango.datasource import *

# ------------------------------------------------------------------------------
module_logger = logging.getLogger(__name__)
module_logger.setLevel("ERROR")

# ------------------------------------------------------------------------------
bokeh_redirected = False

# ------------------------------------------------------------------------------
tango_monitors = dict()
tango_attribute_plots = dict()

# ------------------------------------------------------------------------------
def redirect_bokeh_output(force=False):
    global bokeh_redirected
    if force or not bokeh_redirected:
        try:
            output_notebook(resources=INLINE, hide_banner=True)
            bokeh_redirected = True
        except:
            pass
        
# ------------------------------------------------------------------------------
def open_tango_monitor(**kwargs):
    GenericMonitor.instanciate(**kwargs)
        
# ------------------------------------------------------------------------------
class Monitor(NotebookCellContent):
    """ base class for Tango attribute monitors """

    def __init__(self, **kwargs):
        try:
            # get monitor
            self._mid = kwargs['mid']
            # init super class
            NotebookCellContent.__init__(self, str(self._mid))
            # running in standalone mode
            self._standalone_mode = kwargs.get('standalone_mode', True)
            # refresh period in seconds
            rp = kwargs.get('refresh_period', 1.)
            self._refresh_period = self.__compute_refresh_period(rp)
        except:
            self._remove_self_reference()
            raise

    @staticmethod
    def _remove_self_reference(mid):
        module_logger.debug("Monitor.__remove_self_reference: {}".format(mid))
        try:
            del tango_monitors[mid]
        except Exception as e:
            module_logger.debug(e)

    @staticmethod
    def __compute_refresh_period(refresh_period):
        if not refresh_period:
            refresh_period = 1.
        computed_refresh_period = max(0.1, float(refresh_period))
        computed_refresh_period = min(10., computed_refresh_period)
        return computed_refresh_period

    @property
    def mid(self):
        return self._mid
    
    @property
    def standalone_mode(self):
        return self._standalone_mode

    @property
    def refresh_period(self):
        return self._refresh_period

    def close(self):
        module_logger.debug("Monitor.close: {}".format(self._mid))
        self.cleanup()

    def cleanup(self):
        module_logger.debug("Monitor.cleanup: {}".format(self._mid))
        # remove self ref. from monitors repository
        self._remove_self_reference(self._mid)


# ------------------------------------------------------------------------------
class GenericMonitor(Monitor):
    """ Generic monitor for single Tango attribute """

    @staticmethod
    def instanciate(**kwargs):
        redirect_bokeh_output()
        # fully qualified attribute names
        attrs_arg = kwargs.get('attributes', "")
        fqans = attrs_arg.split(',')
        if not fqans:
            return
        scalars = []
        spectra = []
        images = []
        # separate attributes by data format
        attrs_data_format = tango.FMT_UNKNOWN
        for fqan in fqans:
            try:
                ap = tango.AttributeProxy(fqan)
                ap.ping()
                df = ap.get_config().data_format 
                if df == tango.SCALAR:
                    scalars.append(fqan)
                    attrs_data_format = tango.SCALAR
                elif df == tango.SPECTRUM:
                    spectra.append(fqan)
                    attrs_data_format = tango.SPECTRUM
                elif df == tango.IMAGE:
                    images.append(fqan)
                    attrs_data_format = tango.IMAGE
                else:
                    raise Exception("unknown/unsupported data format for attribute {}".format(fqan))
            except Exception as e:
                raise
        # so far, we only support attributes of the same type
        mixed = (1 if scalars else 0) + (1 if spectra else 0) + (1 if images else 0) 
        if mixed > 1:
            raise Exception("Oops, monitored attributes must be of the same type (scalars, spectra or images)")
        # so far, we can only monitor one image per monitor
        if len(images) > 1:
            raise Exception("Oops, sorry but, so far, we can only monitor one image per monitor")
        mid = hashlib.sha1(kwargs.get('attributes').encode("utf8")).hexdigest()
        # close  existing monitor (if any)
        for mn, mi in tango_monitors.items():
            if mi.mid == mid:
                module_logger.debug("Monitor[{}] exist".format(mi.mid))
                try:
                    module_logger.debug("closing Monitor[{}]...".format(mi.mid))
                    mi.close()
                    module_logger.debug("Monitor[{}] successfully closed".format(mi.mid))
                except:
                    module_logger.exception("failed to close Monitor[{}]".format(mi.mid))
                break
        # instanciate the Monitor
        kwargs['mid'] = mid
        kwargs['attrs_data_format'] = attrs_data_format
        kwargs['attribute_list'] = fqans
        try:
            module_logger.debug("instanciating Monitor for {}".format(attrs_arg))
            m = GenericMonitor(**kwargs)
            tango_monitors[m.mid] = m
            module_logger.debug("Monitor successfully instanciated")
        except Exception as e:
            module_logger.error(e)
            raise

    def __init__(self, **kwargs):
        try:
            # init super class
            Monitor.__init__(self, **kwargs)
            # fully qualified attribute name or alias must be specified
            self._fqans = kwargs.get('attribute_list', [])
            if not self._fqans:
                raise Exception("can't open Tango monitor - no Tango attribute name(s) specified")
            # plot refresh period in seconds
            rp = kwargs.get('refresh_period', 1.)
            kwargs['refresh_period'] = rp
            # history buffer depth (for scalar) in seconds
            hbd = kwargs.get('history_buffer_depth', 120)
            kwargs['history_buffer_depth'] = hbd
            # setup data stream
            self._dsr = self._setup_data_stream(**kwargs)
            if self.standalone_mode:
                kwargs['hbd_slider_enabled'] = kwargs['attrs_data_format'] == tango.SCALAR
                kwargs['reset_button_enabled'] = kwargs['attrs_data_format'] == tango.SCALAR
                self._dsc = DataStreamerController('ctr', self._dsr, **kwargs)
                self._dsc.register_close_callback(self.close)
            else:
                self._dsc = None
        except Exception as e:
            module_logger.debug(e)
            self._dsr = None
            self._dsc = None
            self.cleanup()
            raise

    def _setup_data_stream(self, **kwargs):
        data_sources = []
        for fqan in self._fqans:
            data_sources.append(TangoDataSource(fqan, fqan, kwargs['history_buffer_depth'], kwargs['refresh_period']))
        fmt = kwargs.get('attrs_data_format', tango.FMT_UNKNOWN)
        if fmt == tango.SCALAR:
            tch = ScalarChannel(kwargs['attributes'], data_sources=data_sources, model_properties=kwargs)
        elif fmt == tango.SPECTRUM:
            tch = SpectrumChannel(kwargs['attributes'], data_sources=data_sources, model_properties=kwargs)
        elif fmt == tango.IMAGE:
            tch = ImageChannel(kwargs['attributes'], data_source=data_sources[0], model_properties=kwargs)
        else:
            raise Exception("unknown/unsupported data format for one of the following attributes {}".format(self._fqans))
        dsm = DataStream('dsm', channels=[tch])
        dsr = DataStreamer('dsr', data_streams=[dsm],  **kwargs)
        return dsr

    def _get_data_streamer(self):
        return self._dsr

    @property
    def attribute_name(self):
        return self._fqan

    @property
    def buffer_depth(self):
        return self._buffer_depth

    def close(self):
        module_logger.debug("GenericMonitor.close [{}]".format(self.mid))
        try:
            if self._dsc:
                # avoid recurssive call
                tmp = self._dsc
                self._dsc = None
                tmp.close()
            else:
                self.cleanup()
        except Exception as e:
            module_logger.excpetion("GenericMonitor.close failed! [{}]".format(self.mid))

    def cleanup(self):
        module_logger.debug("GenericMonitor.cleanup [{}]".format(self.mid))
        try:
            # call mother-class' cleanup
            super(GenericMonitor, self).cleanup()
        except Exception as e:
            module_logger.excpetion("GenericMonitor.cleanup failed! [{}]".format(self.mid))


# ------------------------------------------------------------------------------
def plot_tango_attribute(ns):
    redirect_bokeh_output()
    n = ns.attr.count("/")
    if not n:
        ap = tango.AttributeProxy(ns.attr)
        av = ap.read()
        fqan = ap.get_device_proxy().name() + "/" + ap.name()
        title = ns.attr + " [" + fqan + "]"
    elif n == 3:
        dn, _, an = ns.attr.rpartition("/")
        dp = tango.DeviceProxy(dn)
        av = dp.read_attribute(an)
        fqan = ns.attr
    else:
        raise Exception("invalid attribute name specified - expected an alias or something like 'fully/qualified/attribute/name'")
    kwargs = dict()
    kwargs['tools'] = 'pan,wheel_zoom,box_select,reset,hover'
    kwargs['title'] = fqan + ' @ ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    if ns.width is not None:
        kwargs['width'] = ns.width
    if ns.height is not None:
        kwargs['height'] = ns.height
    upsidedown = ns.upsidedown if ns.upsidedown is not None else False
    plot = None
    if av.data_format == tango.AttrDataFormat.SCALAR:
        module_logger.debug(fqan + " = " + str(av.value))
    elif av.data_format == tango.AttrDataFormat.SPECTRUM:
        kwargs['toolbar_location'] = 'above'
        plot = figure(**kwargs)
        plot.line(range(av.value.shape[0]), av.value, line_width=1)
        plot.circle(range(av.value.shape[0]), av.value, size=3)
    elif av.data_format == tango.AttrDataFormat.IMAGE:
        kwargs['toolbar_location'] = 'right'
        lcm = LinearColorMapper(palette=Plasma256)
        ymin = 0 if not upsidedown else av.dim_y
        ymax = av.dim_y if not upsidedown else 0
        plot = figure(x_range=(0, av.dim_x), y_range=(ymin, ymax), **kwargs)
        image = av.value if not upsidedown else av.value[::-1]
        plot.image(image=[image], x=0, y=ymin, dw=av.dim_x, dh=av.dim_y, color_mapper=lcm)
    else:
        module_logger.debug(fqan + " has an unknown/unsupported attribute data format [{}]".format(str(av.data_format)))
    if plot:
        ht = plot.select(HoverTool)[0]
        ht.tooltips = [("x", "$x"), ("y", "$y"), ("z",'@image')]
        plot.toolbar.active_drag = None
        plot.toolbar.active_scroll = None
        plot.toolbar.logo = None
        tango_attribute_plots[fqan] = show(plot, notebook_handle=True)