# ===========================================================================
#  This file is part of the Tango Ecosystem
#
#  Copyright 2017-2020 Synchrotron SOLEIL, St.Aubin, France
#  Copyright 2021-EOT ESRF, Grenoble, France
#
#  This is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Lesser General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
#
#  This is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
#  more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with This.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================================

import time
from datetime import datetime
import logging
import threading
from collections import deque
import numpy as np
import tango
import tornado.ioloop
from jupytango.datasource import *
from jupytango.tango.eventsconsumer import TangoEventSubscriptionForm, TangoEventsConsumer


# ------------------------------------------------------------------------------
module_logger = logging.getLogger(__name__)
module_logger.setLevel("ERROR")
        
# ------------------------------------------------------------------------------
class TimeSeries(object):
    
    def __init__(self, history_buffer_depth=120, initial_buffers_depth=1024):
        self.__history_buffer_depth = float(history_buffer_depth)
        self.reset(initial_buffers_depth)

    def reset(self, initial_buffers_depth=None):
        # TODO: change the impl so that we use only one time buffer
        self.__wrt_ptr = 0
        self.__buffers_length = self.__buffers_length if initial_buffers_depth is None else initial_buffers_depth
        self.__time_buffer = np.empty((self.__buffers_length,), dtype=float)
        self.__time_buffer.fill(np.nan)
        self.__datetime_buffer = np.empty((self.__buffers_length,), dtype=datetime)
        self.__datetime_buffer.fill(None)
        self.__data_buffer = np.empty((self.__buffers_length,), dtype=float)
        self.__data_buffer.fill(np.nan)
    
    def __shift_data(self, arr, num, fill_value=np.nan):
        result = np.empty_like(arr)
        if num > 0:
            result[:num] = fill_value
            result[num:] = arr[:-num]
        elif num < 0:
            result[num:] = fill_value
            result[:num] = arr[-num:]
        else:
            result[:] = arr
        return result

    def history_buffer_depth_changed(self, hbd):
        # THIS FEATURE HAS A HUGE PERFORMANCE COST - LET'S DISABLE IT
        module_logger.warning("TimeSeries.history_buffer_depth_changed has been disabled for performance reasons")
    
    def __apply_buffer_depth_limit(self):
        #print("TimeSeries.history_buffer_depth_changed: shrinking the data...") 
        past_limit =  time.time() - self.__history_buffer_depth
        index = np.searchsorted(self.__time_buffer, past_limit)
        #print("TimeSeries.history_buffer_depth_changed: data will be left-shifted by {}".format(index))
        self.__time_buffer = self.__shift_data(self.__time_buffer, -index, np.nan)
        self.__datetime_buffer = self.__shift_data(self.__datetime_buffer, -index, None)
        self.__data_buffer = self.__shift_data(self.__data_buffer, -index, np.nan)
        self.__wrt_ptr -= index
        #print("TimeSeries.history_buffer_depth_changed: wrt_ptr is now {}".format(self.__wrt_ptr))

    def append(self, t, v):
        dt = 0 if self.__time_buffer[0] is None else t - self.__time_buffer[0]
        if dt > self.__history_buffer_depth:
            #print("TimeSerie.append: dt is greater than history length - dt:{} - history_length:{}".format(dt, self.__history_buffer_depth))
            wrt_idx = self.__wrt_ptr if self.__wrt_ptr < self.__buffers_length else self.__wrt_ptr - 1
            self.__time_buffer[wrt_idx] = t
            self.__datetime_buffer[wrt_idx] = datetime.fromtimestamp(t)
            self.__data_buffer[wrt_idx] = v
            self.__wrt_ptr += 1 if self.__wrt_ptr < self.__buffers_length else 0
            self.__apply_buffer_depth_limit()
        elif self.__wrt_ptr < self.__buffers_length:
            #print("TimeSerie.append: ok, there's still room for new samples - wrt_ptr:{} - buffers_length:{}".format(self.__wrt_ptr, self.__buffers_length))
            self.__time_buffer[self.__wrt_ptr] = t
            self.__datetime_buffer[self.__wrt_ptr] = datetime.fromtimestamp(t)
            self.__data_buffer[self.__wrt_ptr] = v
            self.__wrt_ptr += 1
        else:
            extension_length = max(round(0.25 * self.__buffers_length), 32)
            #print("TimeSerie.append: not enough room for new samples - extending the buffers by {} samples - current buffers length is {})".format(extension_length, self.__buffers_length))
            self.__time_buffer = np.append(self.__time_buffer, [time.time()] * extension_length, 0)
            self.__datetime_buffer = np.append(self.__datetime_buffer, [datetime.now()] * extension_length, 0)
            self.__data_buffer = np.append(self.__data_buffer, [np.nan] * extension_length, 0)
            self.__buffers_length += extension_length
            self.__time_buffer[self.__wrt_ptr] = t
            self.__datetime_buffer[self.__wrt_ptr] = datetime.fromtimestamp(t)
            self.__data_buffer[self.__wrt_ptr] = v
            self.__wrt_ptr += 1
            
    def time(self):
        return self.__datetime_buffer[: 0 if not self.__wrt_ptr else self.__wrt_ptr - 1]
    
    def data(self):
        return self.__data_buffer[: 0 if not self.__wrt_ptr else self.__wrt_ptr - 1]
    
# ------------------------------------------------------------------------------
class MonitoredAttribute(TangoEventsConsumer):
    """ manages a monitored Tango attribute """

    def __init__(self, fqan, history_buffer_depth=300.0, refresh_period=1.0):
        try:
            TangoEventsConsumer.__init__(self)
            # data source url
            self._fqan = fqan
            # the refresh period in seconds (for polled attribute only)
            self._refresh_period = int(1000. * refresh_period)
            # the periodic callback (for polled attribute only)
            self.__periodic_callback = None
            # data source proxy (connection to Tango device server)
            self._proxy = None
            # fetch data source info (data type, data format, ...)
            self._config = None
            # scalar attributes need a particular treatment
            self._is_scalar = False
            self._time_series = None
            # data (i.e. Tango attribute value as an instance of ChannelData)
            self._data = None
            # events stuffs
            self._data_lock = threading.Lock()
            self._has_tango_event = False
            self._event_counter = 0
            # initialization
            self.__initialize(history_buffer_depth)
        except Exception as e:
            module_logger.error(e)

    def __connect(self):
        try:
            # connect to the device
            self._proxy = tango.AttributeProxy(self._fqan)
        except tango.DevFailed as tdf:
            err  = "Failed to connect to Tango attribute '{}'\n".format(self._fqan)
            err += "Please make sure the associated Tango device is up and running"
            err += self.__error_tips(tdf)
            raise Exception(err)
        except Exception as e:
            err  = "Failed to connect to Tango attribute '{}'\n".format(self._fqan)
            err += str(e)
            raise Exception(err)

    def __reset_connection(self):
        self._proxy = None
        
    def __check_device_is_alive(self, val=None):
        if self._proxy is None:
            self.__connect()
        try:
            # be sure the device is running (test connection to Tango device server)
            self._proxy.ping()
        except tango.DevFailed as tdf:
            err  = "Failed to connect to Tango attribute '{}'\n".format(self._fqan)
            err += "Please make sure the associated Tango device is up and running\n"
            err += self.__error_tips(tdf)
            raise Exception(err)
        except Exception as e:
            err  = "Failed to connect to Tango attribute '{}'\n".format(self._fqan)
            err += str(e)
            raise Exception(err)
            
    def __get_attribute_config(self):
        self.__check_device_is_alive()
        try:
            # fetch data source info (data type, data format, ...)
            self._config = self._proxy.get_config()
        except tango.DevFailed as tdf:
            err = "Failed to obtain attribute configuration for '{}'\n".format(self._fqan)
            err += " Please check state the status of the associated device\n"
            raise Exception(self.__append_tango_exception(err, tdf))
        except Exception as e:
            err  = "Failed to obtain attribute configuration for '{}'\n".format(self._fqan)
            err += str(e)
            raise Exception(err)
            
    def __error_tips(self, dev_failed=None):
        replying_to_ping = False
        try:
            self._proxy.ping()
            replying_to_ping = True
        except:
            pass
        err = "There's a problem with the Tango attribute '{}'\n".format(self.name)
        if replying_to_ping:
            err = "The associated Tango device is alive but might be too busy or blocked\n"
            err += "\t`-> check the device state and status'\n"
        else:
            err = "The associated Tango device is NOT alive and might have crashed or been stopped\n"
            err += "\t`-> try to restart the device\n"
        self.__append_tango_exception(dev_failed)
        return err

    @staticmethod
    def __append_tango_exception(error_text, dev_failed=None):
        if dev_failed and isinstance(dev_failed, tango.DevFailed):
            error_text += "\nTango exception caught:\n"
            for de in dev_failed.args:
                error_text += "{}\n".format(de.desc)
        return error_text

    def reset(self):
        module_logger.debug("MonitoredAttribute.reset: resetting data for {}".format(self.name))
        self._time_series.reset()
        
    def __initialize(self, history_buffer_depth=120):
        # reset both proxy and config
        self._proxy = None
        self._config = None
        val = ChannelData(self._fqan)
        try:
            # fetch data source info (data type, data format, ...)
            self.__get_attribute_config()
            # scalar attributes need a circular buffer and a dummy attribute value
            self._is_scalar = self._config.data_format == tango.AttrDataFormat.SCALAR
            if self._is_scalar:
                self._time_series = TimeSeries(history_buffer_depth)
            # store the data format
            self._format = self.__tango_to_channel_format(self._config.data_format)
            # pretend that the attribute doesn't support events and will rely on polling
            self._has_tango_event = False
            # try to subscribe to change event... 
            f = TangoEventSubscriptionForm()
            f.dev = self._proxy.get_device_proxy().name()
            f.attr = self._proxy.name()
            try:
                f.evt_type = tango.EventType.CHANGE_EVENT 
                self.subscribe_event(f)
                self._has_tango_event = True
                module_logger.info("{}: successfully subscribed to CHANGE_EVENT".format(self.name))
            except Exception as e:
                module_logger.info("{}: failed to subscribe to CHANGE_EVENT, trying the PERIODIC one...".format(self.name))
            # in case subscription to CHANGE_EVENT failed try the one PERIODIC 
            if not self._has_tango_event:
                try:
                    f.evt_type = tango.EventType.PERIODIC_EVENT 
                    self.subscribe_event(f)
                    self._has_tango_event = True
                    module_logger.info("{}: successfully subscribed to PERIODIC_EVENT".format(self.name))
                except Exception as e:
                    module_logger.info("{}: failed to subscribe to PERIODIC_EVENT - switching to polling!".format(self.name))
                    # first poll is manual (best effort)
                    try:
                        self.__update_value()
                    except:
                        pass
                    # spawn a periodic callback to poll the attribute
                    self.__periodic_callback = tornado.ioloop.PeriodicCallback(self.__update_value, self._refresh_period) 
                    self.__periodic_callback.start()
        except tango.DevFailed as tdf:
            module_logger.exception("Initialization failed for {}".format(self.name)) 
            val.set_error(err, tdf)
            self.__push_data(val)
        except Exception as e:
            module_logger.exception("Initialization failed for {}".format(self.name)) 
            val.set_error(str(e))
            self.__push_data(val)

    @property
    def name(self):
        return self._fqan

    @property
    def __initialized(self):
        return (self._proxy is not None) and (self._config is not None)

    @property
    def is_valid(self):
        return self.__initialized

    def _handle_event(self, event):
        try:
            #print("received event from {} for {}".format(event.data.attr_name, self.name))
            self._event_counter += 1
            self.__update_value(event)
        except Exception as e:
            print(e)
                
    @staticmethod
    def __tango_to_channel_format(format):
        if format == tango.AttrDataFormat.SCALAR:
            return ChannelData.Format.SCALAR
        elif format == tango.AttrDataFormat.SPECTRUM:
            return ChannelData.Format.SPECTRUM
        elif format == tango.AttrDataFormat.IMAGE:
            return ChannelData.Format.IMAGE
        else:
            return ChannelData.Format.UNKNOWN

    def __update_value(self, event=None):
        val = ChannelData(self._fqan)
        try:
            if event is None:
                #module_logger.debug("MonitoredAttribute:__update_value: polling the attribute...")
                try:
                    # read data source (i.e. read Tango attribute value)
                    av = self._proxy.read()
                except Exception as e:
                    self._time_series.append(time.time(), np.nan)
                    err = "Failed to read Tango attribute '{}'\n".format(self._fqan)
                    err += self.__error_tips()
                    raise Exception(err)
            else:
                #module_logger.debug("MonitoredAttribute:__update_value: dealing with event...")
                # is the Tango event carrying an error?
                if event.data.err:
                    self.__reset_connection()
                    err = event.data.errors[0].desc + "\n"
                    err += self.__error_tips()
                    raise Exception(err)
                # no error at event level - any attribute value level?
                av = event.data.attr_value
            # deal with bad values
            if av.has_failed or av.is_empty:
                av.value = np.nan
                av.time = tango.TimeVal(time.time())
            # ok, we have a valid attribute value convert it to our ChannelData representation
            if self._is_scalar:
                # inject the data in to the underlying ring buffers
                self._time_series.append(av.time.totime(), float(av.value))
                # pass a copy of the data to the consumer
                val.set_data(self._time_series.time(), 
                             self._time_series.data(), 
                             self._format)
            else:
                val.set_data(np.asarray([datetime.fromtimestamp(av.time.totime())]), 
                             np.asarray([av.value]), 
                             self._format)
            #print("successfully updated value for {}".format(self.name))
        except Exception as e:
            module_logger.exception("Failed to update value for {}".format(self.name))
            val.set_error(str(e))
        finally:
            #print("pushing value for {} - has valid data:{}".format(self.name, val.has_valid_data))
            self.__push_data(val)
            #print("value pushed for {}".format(self.name))

    def __push_data(self, value):
        with self._data_lock:
            self._data = value
      
    def __push_data_if_none(self, value):
        with self._data_lock:
            if self._data is None:
                self._data = value
            
    def __pop_data(self):
        with self._data_lock:
            current_data = self._data
            self._data = None
            return current_data

    def get_last_value(self, force=False):
        return self.__pop_data()
        
    def is_scalar(self):
        return self.__is_a(tango.AttrDataFormat.SCALAR)

    def is_spectrum(self):
        return self.__is_a(tango.AttrDataFormat.SPECTRUM)

    def is_image(self):
        return self.__is_a(tango.AttrDataFormat.IMAGE)

    def __is_a(self, fmt):
        if not self.has_valid_configuration():
            self.__get_attribute_config()
        return self._config.data_format == fmt

    def has_valid_configuration(self, try_reconnect=True):
        if not self._config and try_reconnect:
            try:
                self.__initialize()
            except:
                return False
        return False if not self._config else True

    def refresh_period_changed(self, up):
        if not self.__periodic_callback:
            return
        self._refresh_period = max(10, int(1000. * up))
        self._refresh_period = min(6000, int(1000. * up))
        module_logger.debug("MonitoredAttribute:refresh_period_changed:changing the update period to {} ms".format(self._refresh_period))
        try:
            self.__periodic_callback.stop()
            self.__periodic_callback = tornado.ioloop.PeriodicCallback(self.__update_value, self._refresh_period) 
            self.__periodic_callback.start()
        except:
            module_logger.exception("MonitoredAttribute:refresh_period_changed failed!")
     
    
    def history_buffer_depth_changed(self, hbd):
        if self._time_series is not None:
            self._time_series.history_buffer_depth_changed(hbd)
            
    def cleanup(self):
        try:
            self.__periodic_callback.stop()
            self.__periodic_callback = None
        except:
            pass
        try:
            self.unsubscribe_events()
        except:
            pass
        try:
            self.__reset_connection()
        except:
            pass


# ------------------------------------------------------------------------------
class TangoDataSource(DataSource):
    
    def __init__(self, dtsn, fqan, history_buffer_depth=0, refresh_period=1.0):
        DataSource.__init__(self, dtsn)
        self._mta = MonitoredAttribute(fqan, history_buffer_depth, refresh_period)

    @property
    def monitored_attribute(self):
        return self._mta

    def refresh_period_changed(self, up):
        self._mta.refresh_period_changed(up)
    
    def history_buffer_depth_changed(self, hbd):
        self._mta.history_buffer_depth_changed(hbd)
        
    def pull_data(self):
        return self._mta.get_last_value()

    def is_scalar(self):
        return self._mta.is_scalar()

    def is_spectrum(self):
        return self._mta.is_spectrum()

    def is_image(self):
        return self._mta.is_image()

    def reset(self):
        self._mta.reset()
        
    def cleanup(self):
        self._mta.cleanup()



