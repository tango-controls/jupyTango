# ===========================================================================
#  This file is part of the Tango Ecosystem
#
#  Copyright 2017-2020 Synchrotron SOLEIL, St.Aubin, France
#  Copyright 2021-EOT ESRF, Grenoble, France
#
#  This is free software: you can redistribute it and/or modify it under the
#  terms of the GNU Lesser General Public License as published by the Free
#  Software Foundation, either version 3 of the License, or (at your option)
#  any later version.
#
#  This is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
#  more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with This.  If not, see <http://www.gnu.org/licenses/>.
# ===========================================================================

from collections import OrderedDict, deque
from math import ceil, pi
import math
import copy

import math as mt
import numpy as np

from IPython.display import display

import ipywidgets as ipw

from bokeh.layouts import row, column, layout, gridplot
from bokeh.models import ColumnDataSource, DatetimeTickFormatter, LegendItem, Label, DataRange1d
from bokeh.models import widgets as BokehWidgets
from bokeh.models.glyphs import Rect
from bokeh.models.mappers import LinearColorMapper
from bokeh.models.ranges import Range1d
from bokeh.models.tools import BoxSelectTool, HoverTool, CrosshairTool
from bokeh.models.tools import ResetTool, PanTool, BoxZoomTool
from bokeh.models.tools import WheelZoomTool, SaveTool
from bokeh.models.scales import LinearScale
from bokeh.models.axes import LinearAxis
from bokeh.palettes import Plasma256
from bokeh.plotting import figure
import bokeh.util.version as BokehVersion
import bokeh.events

from jupytango.tools import *
from jupytango.datasource import *
from jupytango.session import BokehSession

rescaler = None 
try:
    import cv2
    has_opencv = True
    rescaler = cv2.resize
except:
    has_opencv = False
    try:
        import skimage.transform
        rescaler = skimage.transform.rescale 
        has_skimage = True
    except:
        has_skimage = False

        
# ------------------------------------------------------------------------------
module_logger = logging.getLogger(__name__)
module_logger.setLevel("ERROR")


# ------------------------------------------------------------------------------
class Children(OrderedDict):
    def __init__(self, owner, obj_class):
        OrderedDict.__init__(self)
        self._owner = owner
        self._obj_class = obj_class
        self._add_callbacks = OrderedDict()

    def register_add_callback(self, cb):
        if cb and hasattr(cb, '__call__'):
            l = len(self._add_callbacks)
            self._add_callbacks[l + 1] = cb

    def call_add_callbacks(self, child):
        for cb in self._add_callbacks.values():
            try:
                cb(child)
            except:
                pass

    def add(self, children):
        if isinstance(children, (list, tuple)):
            for c in children:
                self.__add_child(c)
        else:
            self.__add_child(children)

    def __add_child(self, child):
        if child is None:
            return
        if child is self._owner:
            err = "invalid argument: can't add 'self' to children"
            raise ValueError(err)
        if isinstance(child, self._obj_class):
            if not len(self):
                self._master_child = child
            self[child.name] = child
            self.call_add_callbacks(child)
        else:
            ic = child.__class__
            ec = self._obj_class.__name__
            err = "invalid argument: expected an iterable collection or a single instance of {} - got {}".format(ec, ic)
            raise ValueError(err)


# ------------------------------------------------------------------------------
class UIEvent(object):
    """User Interface event"""

    Type = enum(
        'SELECTION_CHANGED',
        'RESET_CLICKED',
        'UNKNOWN'
    )

    
# ------------------------------------------------------------------------------
class DataStreamEvent(object):
    """Data stream event"""

    Type = enum(
        'ERROR',
        'RECOVER',
        'MODEL_CHANGED',
        'EOS',
        'UI',
        'UNKNOWN'
    )

    def __init__(self, event_type=Type.UNKNOWN, emitter=None, data=None, error=None, exception=None):
        # evt. type
        self.type = event_type
        # uuid of the evt. emitter
        self.emitter = emitter
        # abstract event data
        self.data = data
        # error text
        self.error = error
        # exception
        self.exception = exception


# ------------------------------------------------------------------------------
class DataStreamEventHandler(object):
    """Data stream event handler"""

    supported_events = [
        DataStreamEvent.Type.ERROR,
        DataStreamEvent.Type.RECOVER,
        DataStreamEvent.Type.MODEL_CHANGED,
        DataStreamEvent.Type.EOS,
        DataStreamEvent.Type.UI
    ]

    def __init__(self, name):
        self._name = name
        # callbacks
        self._callbacks = dict()
        for event in self.supported_events:
            self._callbacks[event] = set()

    @property
    def name(self):
        return self._name

    def register_event_handler(self, event_handler, events):
        assert (isinstance(events, (list, tuple)))
        assert (isinstance(event_handler, DataStreamEventHandler))
        for event in events:
            # print("{}.{}: registering event handler {} for event {}".format(self.__class__, self.name, event_handler.name, event))
            self._callbacks[event].add(event_handler)

    def dump_event_handler(self):
        for event in self.supported_events:
            # print("{}.{} has {} event handlers for event {}".format(self.__class__, self.name, len(self._callbacks[event]), event))
            for event_handler in self._callbacks[event]:
                event_handler.dump_event_handler()

    def __emit(self, event):
        try:
            assert(isinstance(event, DataStreamEvent))
            for event_handler in self._callbacks[event.type]:
                module_logger.info("DataStreamEventHandler.{}: emitting event {} towards {}".format(self.name, event.type, event_handler.name))
                event_handler.__handle_stream_event(event)
                # print("3")
            # print("4")
        except IndexError:
            pass
        except Exception as e:
            module_logger.exception("Exception caught!")

    def __handle_stream_event(self, event):
        try:
            self.handle_stream_event(event)
        except:
            pass
        finally:
            self.__propagate(event)

    def __propagate(self, event):
        assert (isinstance(event, DataStreamEvent))
        # print("{}: propagating event {} ".format(self.name, event.type))
        self.__emit(event)

    def emit_error(self, sd):
        try:
            assert (isinstance(sd, ChannelData))
            module_logger.info("{}.{}: emitting DataStreamEvent.Type.ERROR".format(self.__class__, self._name))
            self.dump_event_handler()
            evt = DataStreamEvent(DataStreamEvent.Type.ERROR, self.uid, None, sd.error, sd.exception)
            self.__emit(evt)
        except Exception as e:
            module_logger.exception("Exception caught!")

    def emit_recover(self):
        evt = DataStreamEvent(DataStreamEvent.Type.RECOVER, self.uid)
        self.__emit(evt)

    def emit_model_changed(self, model):
        evt = DataStreamEvent(DataStreamEvent.Type.MODEL_CHANGED, self.uid, model)
        # print("{}: emitting model changed evt".format(self.name))
        self.__emit(evt)

    def handle_stream_event(self, event):
        pass
    
    
# ------------------------------------------------------------------------------
class InteractionsManager(object):
    def __init__(self):
        self._session = None
        self._reset_callback = None
        self._range_change_callback = None
        self._range_change_notified = False

    def setup(self, session, figure, range_change_callback, reset_callback=None):
        assert (isinstance(session, BokehSession))
        self._session = session
        self._range_change_callback = range_change_callback
        self._reset_callback = reset_callback
        figure.x_range.on_change('start', self.__on_range_change)
        figure.x_range.on_change('end', self.__on_range_change)
        figure.y_range.on_change('start', self.__on_range_change)
        figure.y_range.on_change('end', self.__on_range_change)
        if self._reset_callback:
            figure.on_event(bokeh.events.Reset, self.__on_reset)
            
    def __on_reset(self, event):
        if self._reset_callback:
            self._session.timeout_callback(self._reset_callback, 0.25)

    def __on_range_change(self, attr, old, new):
        self.__notify_range_change()

    def __notify_range_change(self):
        if not self._range_change_notified and self._range_change_callback:
            self._range_change_notified = True
            try:
                # -----------------------------------------------------------------------------
                # InteractionsManager.__on_range_change is called with 'document' locked
                # we consequently have to call the owner's handler asynchrounously so that
                # it will be able to update the plot
                # -----------------------------------------------------------------------------
                # nb: this a tmp impl - we are waiting for the bokeh events to improve it
                # -----------------------------------------------------------------------------
                self._session.timeout_callback(self._range_change_callback, 0.25)
            except Exception as e:
                module_logger.exception("Exception caught!")

    def range_change_handled(self):
        self._range_change_notified = False


# ------------------------------------------------------------------------------
ScaleType = enum(
    'INDEXES',
    'RANGE',
    'DATA_SOURCE'
)

# ------------------------------------------------------------------------------
class Scale(object):
    """a scale"""

    def __init__(self, **kwargs):
        self._start = kwargs.get('start', None)
        self._end = kwargs.get('end', None)
        self._num_points = kwargs.get('num_points', None)
        self._label = kwargs.get('label', None)
        self._unit = kwargs.get('unit', None)
        self._channel = kwargs.get('channel', None)
        if self._channel is not None and len(self._channel):
            self._type = ScaleType.DATA_SOURCE
        elif self._start is None or self._end is None or self._num_points is None:
            self._type = ScaleType.INDEXES
        else:
            self._type = ScaleType.RANGE
        self._array, self._step = self.__compute_linear_space()
        # the following will be used for linear interpolation (point coords -> pixel index)
        self._ix = self._array
        self._iy = np.linspace(0, self._array.shape[0] - 1, num=self._array.shape[0], dtype=int)

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, t):
        raise Exception("Scale.type is immutable - can't change its value")

    @property
    def start(self):
        return self._start

    @start.setter
    def start(self, s):
        raise Exception("Scale.start is immutable - can't change its value")

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, e):
        raise Exception("Scale.end is immutable - can't change its value")

    @property
    def num_points(self):
        return self._num_points

    @num_points.setter
    def num_points(self, np):
        raise Exception("Scale.num_points is immutable - can't change its value")

    @property
    def step(self):
        return self._step

    @step.setter
    def step(self, s):
        raise Exception("Scale.step is immutable - can't change its value")

    @property
    def label(self):
        return self._label

    @label.setter
    def label(self, label):
        self._label = label

    @property
    def unit(self):
        return self._unit

    @unit.setter
    def unit(self, unit):
        self._unit = unit

    @property
    def channel(self):
        return self._channel

    @channel.setter
    def channel(self, c):
        raise Exception("Scale.channel is immutable - can't change its value")

    @property
    def array(self):
        return self._array

    @array.setter
    def array(self, a):
        raise Exception("Scale.array is immutable - can't change its value")

    @property
    def range(self):
        return tuple([self._start, self._end, self._num_points])

    @property
    def bokeh_range(self):
        return Range1d(self._start, self._end) if self.__validate_range() else Range1d(-1, 1)

    def range_coords_to_indexes(self, start_p, end_p):
        start_i = mt.floor(np.interp(start_p, self._ix, self._iy))
        end_i = mt.ceil(np.interp(end_p, self._ix, self._iy)) + 1
        return start_i, end_i

    def validate(self):
        if self._type != ScaleType.INDEXES:
            self.__validate_range()
            self.__validate_num_points()

    def __validate_range(self):
        if self._start is not None and self._end is not None and self._start == self._end:
            raise ValueError("invalid axis scale: the specified 'range' is empty")

    def __validate_num_points(self):
        if self._start is not None and self._end is not None and self._num_points is not None and self._num_points < 1:
            raise ValueError("invalid axis scale: the specified 'num_points' is invalid")

    def __compute_linear_space(self):
        try:
            array, step = np.linspace(float(self._start),
                                      float(self._end),
                                      int(self._num_points),
                                      endpoint=True,
                                      retstep=True)
        except:
            array, step = np.zeros((0,)), 0.
        return array, step

    def has_valid_scale(self):
        valid_range = self._start is not None and self._end is not None and self._start != self._end
        return valid_range and self._num_points is not None and self._num_points >= 1

    def axis_label(self):
        label = self._label
        unit = self._unit
        axis_label = ''
        if label:
            axis_label = label
        if unit:
            axis_label += ' [' if len(label) else ''
            axis_label += unit
            axis_label += ']' if len(label) else ''
        return None if not len(axis_label) else axis_label

    def __repr__(self):
        return "scale:{} type:{} start:{} end:{} np:{} step:{} unit:{} channel:{}".format(self.label,
                                                                                          self.type,
                                                                                          self.start,
                                                                                          self.end,
                                                                                          self.num_points,
                                                                                          self.step,
                                                                                          self.unit,
                                                                                          self.channel)

    
# ------------------------------------------------------------------------------
class ModelHelper(object):
    line_colors = {
        0: 'darkblue',
        1: 'crimson',
        2: 'darkgreen',
        3: 'black',
        4: 'darkorchid',
        5: 'darkorange',
        6: 'deepskyblue',
        7: 'slategrey',
        8: 'gold',
        9: 'magenta'
    }

    @staticmethod
    def line_color(index):
        i = index % 10
        return ModelHelper.line_colors[i]

    @staticmethod
    def plot_style(instance, index):
        i = index % 3
        if i == 0:
            return instance.circle
        if i == 1:
            return instance.square
        if i == 2:
            return instance.diamond
        return instance.square


# ------------------------------------------------------------------------------
class Channel(NotebookCellContent, DataStreamEventHandler):
    """single data stream channel"""

    def __init__(self, name, data_sources=None, model_properties=None):
        NotebookCellContent.__init__(self, name, logger=module_logger)
        DataStreamEventHandler.__init__(self, name)
        # associated bokeh session
        self._session = None
        # data sources
        self._bad_data_source_cnt = 0
        self._data_sources_health = {}
        self._disabled_data_source = {}
        self._data_sources = Children(self, DataSource)
        self.add_data_sources(data_sources)
        # model properties
        self._model_props = dict() if model_properties is None else copy.deepcopy(model_properties)
        # tmp label
        self._msg_label = None

    def handle_stream_event(self, event):
        pass

    @property
    def title(self):
        return self._model_props.get('channel_title', None)

    @property
    def show_title(self):
        return self._model_props.get('show_channel_title', False)

    @property
    def data_source(self):
        """returns the 'first' (and sometimes 'unique') data source"""
        for ds in self._data_sources.values():
            return ds
        return None

    @property
    def data_sources(self):
        """returns the dict of data sources"""
        return self._data_sources

    def set_data_source(self, ds):
        """set the channel unique data source"""
        if ds is not None:
            assert (isinstance(ds, DataSource))
            self._data_sources.clear()
            self.add_data_source(ds)

    def add_data_source(self, ds):
        """add the specified data source to the channel"""
        if ds is not None:
            assert (isinstance(ds, DataSource))
            self._data_sources[ds.name] = ds
            self._data_sources_health[ds.name] = True

    def add_data_sources(self, ds):
        """add the specified data source to the channel"""
        if ds is not None:
            assert (isinstance(ds, (list, tuple)))
            for s in ds:
                self.add_data_source(s)
                
    def is_data_source_healthy(self, ds_name):
        """is the specified data source up & ready & producing valid data?"""
        return self._data_sources_health[ds_name]
           
    def set_data_source_health(self, ds_name, healthy):
        """mark specified data source as healthy or not"""
        self._data_sources_health[ds_name] = healthy
    
    def get_data(self):
        """returns a dict containing the data of each data source"""
        return {dsn:dsi.pull_data() for dsn, dsi in self._data_sources.items()}

    def cleanup(self):
        """cleanup data sources"""
        for dsn, dsi in self._data_sources.items():
            try:
                module_logger.debug("DataStream channel: cleaning up DataSource {}".format(dsn))
                dsi.cleanup()
            except Exception as e:
                module_logger.exception("Exception caught!")

    @property
    def bokeh_session(self):
        """returns the dict of model properties"""
        return self._session

    @bokeh_session.setter
    def bokeh_session(self, bks):
        """bokeh_session"""
        assert (isinstance(bks, BokehSession))
        self._session = bks

    @property
    def model_properties(self):
        """returns the dict of model properties"""
        return self._model_props

    @model_properties.setter
    def model_properties(self, mp):
        """set the dict of model properties"""
        self._model_props = mp

    @staticmethod
    def _merge_properties(mp1, mp2, overwrite=False):
        if mp1 is None:
            props = mp2 if mp2 is not None else dict()
        elif mp2 is None:
            props = mp1
        else:
            props = mp1
            for k, v in mp2.items():
                if overwrite or k not in mp1:
                    props[k] = v
        return props

    def setup_model(self, **kwargs):
        """asks the channel to setup then return its Bokeh associated model - returns None if no model"""
        return None

    def _show_msg_label(self, mdl, x=70, y=70, text='Waiting for data'):
        self._msg_text = text
        self._msg_cnt = 0
        self._msg_label = Label(
            x=70,
            y=70,
            x_units='screen',
            y_units='screen',
            text=self._msg_text,
            background_fill_alpha=0.0,
            text_font='helvetica',
            text_font_style='italic',
            text_font_size='11pt',
            text_color='black'
        )
        mdl.add_layout(self._msg_label)

    def _animate_msg_label(self):
        try:
            if self._msg_label:
                self._msg_label.text = self._msg_text + "." * self._msg_cnt
                self._msg_cnt = (self._msg_cnt + 1) % 4
        except Exception as e:
            pass

    def _hide_msg_label(self):
        if self._msg_label:
            self._msg_label.visible = False

    def get_model(self):
        """returns the Bokeh model (figure, layout, ...) associated with the Channel or None if no model"""
        return None

    def refresh_period_changed(self, up):
        """tell the datasources that the plots update period changed"""
        for ds in self._data_sources.values():
            try:
                module_logger.debug("Channel.refresh_period for DataSource {}".format(ds.name))
                ds.refresh_period_changed(up)
            except Exception as e:
                module_logger.exception("Exception caught!")

    def history_buffer_depth_changed(self, hbd):
        """tell the datasources that the plots update period changed"""
        for ds in self._data_sources.values():
            try:
                module_logger.debug("Channel.refresh_period for DataSource {}".format(ds.name))
                ds.history_buffer_depth_changed(hbd)
            except Exception as e:
                module_logger.exception("Exception caught!")

    def reset(self):
        """ask the datasources to reset themself"""
        for ds in self._data_sources.values():
            try:
                module_logger.debug("Channel.reset for DataSource {}".format(ds.name))
                ds.reset()
            except Exception as e:
                module_logger.exception("Exception caught!")
                
    def update(self):
        """gives the Channel a chance to update itself"""
        pass
    
    def disable_data_source(self, dsn):
        try:
            self._disabled_data_source[dsn] = True
            self.bokeh_session.safe_document_modifications(lambda: self.data_source_disabled(dsn))
        except:
            pass
        
    def enable_data_source(self, dsn):
        try:
            self._disabled_data_source[dsn] = False
            self.bokeh_session.safe_document_modifications(lambda: self.data_source_enabled(dsn))
        except:
            pass
        
    def data_source_enabled(self, dsn):
        pass
    
    def data_source_disabled(self, dsn):
        pass 
    
    def is_data_source_disabled(self, dsn):
        try:
            return self._disabled_data_source[dsn]
        except:
            return False
    
# ------------------------------------------------------------------------------
class ScalarChannel(Channel):
    """scalar data source channel"""
    
    def __init__(self, name, data_sources=None, model_properties=None):
        Channel.__init__(self, name, data_sources=data_sources, model_properties=model_properties)
        self.__reinitialize()

    def __reinitialize(self):
        # column data source
        self._cds = {} 
        # model
        self._mdl = None   
        # renderers (i.e. y line glyphs)
        self._lrdr = dict()  
        # renderers (i.e. y diamond glyphs)
        self._drdr = dict()
        # y axis 
        self._y_axes = dict()
        
    def get_model(self):
        return self._mdl

    def get_y_axes(self):
        return self._y_axes
    
    def __setup_cds(self, data_source=None):
        columns = {}
        # add an entry for timestamp
        columns['time'] = np.zeros(1)
        # using per data source cds?
        if data_source is None:
            # add an entry for each data source
            for cn, ci in self._data_sources.items():
                columns[cn] = np.zeros(1)
        else:
            # add an entry the specified data source
            columns[data_source] = np.zeros(1)     
        return ColumnDataSource(data=columns)
    
    def __setup_legend_items(self):
        legend_items = []
        for data_source in self.data_sources:
            if not self.is_data_source_disabled(data_source):
                legend_items.append(LegendItem(label=data_source, renderers=[self._lrdr[data_source], self._drdr[data_source]]))
        return legend_items
        
    def __setup_legend(self, figure):
        figure.legend.location = 'top_left'
        figure.legend.click_policy = 'hide'
        figure.legend.items = self.__setup_legend_items()
        
    def __setup_toolbar(self, figure):
        tt = [
            ("source", "$name"),
            ("index", "$index"),
            ("timestamp", "@time{%Y-%m-%d %H:%M:%S.%3N}"),
            ("value", "$y")
        ]
        ht = HoverTool(tooltips=tt, formatters={'@time': 'datetime'})
        figure.add_tools(ht)
        figure.add_tools(PanTool())
        figure.add_tools(BoxZoomTool())
        figure.add_tools(WheelZoomTool())
        figure.add_tools(ResetTool())
        figure.add_tools(SaveTool())
        figure.add_tools(CrosshairTool())
        figure.toolbar.autohide = False
        figure.toolbar.logo = None
        figure.toolbar.active_drag = None
        figure.toolbar.active_scroll = None
        figure.toolbar.active_tap = None
        figure.toolbar.active_inspect = ht

    def __setup_figure(self, **kwargs):
        fkwargs = dict()
        fkwargs['output_backend'] = 'webgl'
        fkwargs['width'] = kwargs.get('width', 950)
        fkwargs['height'] = kwargs.get('height', 250)
        fkwargs['toolbar_location'] = 'above'
        fkwargs['tools'] = ''
        fkwargs['name'] = str(kwargs.get('uid', self.uid))
        multi_y_axis = kwargs.get('multi_y_axis', True)
        fkwargs['x_axis_type'] = 'datetime'
        if not multi_y_axis:
            fkwargs['y_axis_type'] = kwargs.get('y_axis_type', "linear")
        if not multi_y_axis:
            y_range = kwargs.get('y_range', None)
            if y_range is not None and isinstance(y_range, tuple):
                fkwargs['y_range'] = y_range
        f = figure(**fkwargs)
        dtf = DatetimeTickFormatter()
        dtf.milliseconds = "%H:%M:%S:%3N"
        dtf.seconds = "%H:%M:%S:%3N"
        dtf.minutes = "%H:%M:%S"
        dtf.hours = "%H:%M:%S"
        dtf.days = "%d:%H:%M"
        dtf.months = "%d %m %Y %H %M %S"
        dtf.years = "%d %m %Y %H %M %S"
        f.xaxis.formatter = dtf
        f.xaxis.major_label_orientation = pi / 4
        if multi_y_axis:
            f.yaxis.visible = False
        else:
            y_axis_label = kwargs.get('y_axis_label', None)
            if y_axis_label is not None:
                f.yaxis.axis_label = y_axis_label
        layout = kwargs.get('layout', 'column')
        if kwargs['show_title'] and layout != 'tabs':
            f.title.text = self.name
        return f

    def __setup_glyphs(self, figure, data_source, show_legend=True, multi_y_axis=True):
        kwargs = dict()
        kwargs['x'] = 'time'
        kwargs['y'] = data_source
        kwargs['source'] = self._cds[data_source]
        kwargs['name'] = data_source
        if multi_y_axis:
            kwargs['y_range_name'] = data_source
        kwargs['line_color'] = ModelHelper.line_color(len(self._lrdr))
        kwargs['line_width'] = 1
        if show_legend:
            kwargs['legend_label'] = data_source
        l = figure.line(**kwargs)
        kwargs['fill_color'] = kwargs['line_color'] 
        c = figure.diamond(**kwargs)
        return l,c

    @tracer
    def setup_model(self, **kwargs):
        """asks the channel to setup then return its Bokeh associated model - returns None if no model"""
        try:
            self._mdl = None
            props = self._merge_properties(self.model_properties, kwargs)
            # instanciate the ColumnDataSources (one per data source)
            for data_source in self.data_sources:
                self._cds[data_source] = self.__setup_cds(data_source)
            # setup figure
            show_title = True if len(self.data_sources) == 1 else False
            show_title = props.get('show_title', show_title)
            props['show_title'] = show_title
            f = self.__setup_figure(**props)
            # setup glyphs
            show_legend = False if len(self.data_sources) == 1 else True
            show_legend = props.get('show_legend', show_legend)
            multi_y_axis = props.get('multi_y_axis', True)
            if multi_y_axis:
                i = 0 
                y_range = props.get('y_range', None)
                if not isinstance(y_range, dict):
                    y_range = {}
                y_axis_type = kwargs.get('y_axis_type', None)
                if not isinstance(y_axis_type, dict):
                    y_axis_type = {}
                for data_source in self.data_sources:
                    if self.is_data_source_disabled(data_source):
                        self.disable_data_source(data_source)
                    ud_range = y_range.get(data_source, None)
                    ds_range = DataRange1d() if ud_range is None else DataRange1d(start=ud_range[0], end=ud_range[1]) 
                    f.extra_y_ranges[data_source] = ds_range
                    ud_axis_type = y_axis_type.get(data_source, "linear") 
                    if ud_axis_type == "log":
                        self._y_axes[data_source] = LogAxis(y_range_name=data_source, axis_label=data_source)
                    else:
                        self._y_axes[data_source] = LinearAxis(y_range_name=data_source, axis_label=data_source)
                    f.add_layout(self._y_axes[data_source], "left" if not i%2 else "right")
                    self._lrdr[data_source], self._drdr[data_source] = self.__setup_glyphs(f, data_source, show_legend, multi_y_axis)
                    f.extra_y_ranges[data_source].renderers = [self._lrdr[data_source]]
                    i += 1
            else:
                for data_source in self.data_sources:
                    if self.is_data_source_disabled(data_source):
                        self.disable_data_source(data_source)
                    self._lrdr[data_source], self._drdr[data_source] = self.__setup_glyphs(f, data_source, show_legend, multi_y_axis)
            # setup the legend
            if show_legend:
                self.__setup_legend(f)
            # show tmp label
            self._show_msg_label(f)
            # setup the toolbar
            self.__setup_toolbar(f)
            # store figure
            self._mdl = f
        except Exception as e:
            module_logger.exception(e)
        return self._mdl

    def show_healthy_data_source(self, data_source, data_source_was_healthy):
        try:
            if not data_source_was_healthy:
                self._bad_data_source_cnt -= 1
                self._lrdr[data_source].visible = not self.is_data_source_disabled(data_source)
                self._drdr[data_source].visible = not self.is_data_source_disabled(data_source)
            self.set_data_source_health(data_source, True)
        except Exception as e:
            module_logger.exception("ScalarChannel.show_healthy_data_source failed!")

    def hide_broken_data_source(self, data_source, data_source_was_healthy):
        try:
            if data_source_was_healthy:
                self._bad_data_source_cnt += 1
                self._lrdr[data_source].visible = False
                self._drdr[data_source].visible = False
            self.set_data_source_health(data_source, False)
        except Exception as e:
            module_logger.exception("ScalarChannel.hide_broken_data_source failed!")
       
    def data_source_enabled(self, data_source):
        visible = self.is_data_source_healthy(data_source)
        self._lrdr[data_source].visible = visible
        self._drdr[data_source].visible = visible
        self.__setup_legend(self._mdl)
        try:
            self._y_axes[data_source].visible = visible
        except:
            pass
        
    def data_source_disabled(self, data_source):
        self._lrdr[data_source].visible = False
        self._drdr[data_source].visible = False
        self.__setup_legend(self._mdl)
        try:
            self._y_axes[data_source].visible = False
        except:
            pass
        
    def update(self):
        """gives each Channel a chance to update itself (e.g. to update the ColumDataSources)"""
        # print('scalar channel update')
        dsd = {}
        try:
            # get data from each channel
            previous_bad_data_source_cnt = self._bad_data_source_cnt
            for sn, si in self._data_sources.items():
                ds_was_healthy = self.is_data_source_healthy(sn)
                ds_data = si.pull_data()
                if ds_data is None:
                    # no new data from data source, jump to next one
                    continue
                if ds_data.has_error:
                    self.hide_broken_data_source(sn, ds_was_healthy)
                    module_logger.info("ScalarChannel: emitting error cause data source {} returned bad data...".format(sn))
                    self.emit_error(ds_data)
                elif not ds_data.has_valid_data:
                    self.hide_broken_data_source(sn, ds_was_healthy)
                    continue
                else:
                    time_buffer_len = ds_data.time.shape[0]
                    data_buffer_len = ds_data.data.shape[0]
                    if time_buffer_len != data_buffer_len:
                        self.hide_broken_data_source(sn, ds_was_healthy)
                        msg = "ScalarChannel: data source returned inconsistent data (time and data buffers must have the same length)"
                        ds_data.set_error(self, msg)
                        module_logger.error(msg)
                        module_logger.info("ScalarChannel: emitting error")
                        self.emit_error(ds_data)
                        continue
                    self.show_healthy_data_source(sn, ds_was_healthy)
                    dsd[sn] = ds_data
            if not self._bad_data_source_cnt:
                self._hide_msg_label()
                if previous_bad_data_source_cnt:
                    module_logger.info("ScalarChannel: emitting recover")
                    self.emit_recover()
            time_scale_set = False
            for sn, sd in dsd.items():  
                if sd.has_valid_data:
                    self._cds[sn].data.update({'time':sd.time, sn:sd.data})              
        except Exception as e:
            module_logger.exception(e)

    def cleanup(self):
        self.__reinitialize()
        super(ScalarChannel, self).cleanup()
    
# ------------------------------------------------------------------------------
class SpectrumChannel(Channel):
    """spectrum data source channel"""

    def __init__(self, name, data_sources=None, model_properties=None):
        Channel.__init__(self, name, data_sources=data_sources, model_properties=model_properties)
        self.__reinitialize()

    def __reinitialize(self):
        self._cds = None    # column data source
        self._xsn = None    # x scale name #TODO: inject name into scale class
        self._xsc = None    # x scale
        self._ysc = None    # y scale
        self._mdl = None    # model
        self._rdr = dict()  # renderers (i.e. y glyphs)
        
    def get_model(self):
        """returns the Bokeh model (figure, layout, ...) associated with the Channel or None if no model"""
        return self._mdl

    def __setup_cds(self):
        columns = OrderedDict()
        # add an entry for x scale data (for indexes or range scales)
        columns[self._xsn] = np.zeros(1)
        # add an entry for each child
        for cn, ci in self._data_sources.items():
            columns[cn] = np.zeros(1)
        return ColumnDataSource(data=columns)

    def __validate_x_channel(self):
        xsn = self._xsc.channel
        if self._xsc.type == ScaleType.DATA_SOURCE and (xsn is None or not xsn in self.data_sources):
            err = "invalid SpectrumChannel configuration: the specified 'x' scale channel" \
                  " '{}' must be one of the {} data sources".format(xsn, self.name)
            raise Exception(err)
        if xsn is None or not len(xsn):
            xsn = self.__generate_x_channel_name()
        return xsn

    def __generate_x_channel_name(self):
        x_channel_name = 'x'
        while x_channel_name in self.data_sources:
            x_channel_name += 'x'
        return x_channel_name

    def __setup_legend(self, mdl):
        mdl.legend.location = 'top_left'
        mdl.legend.click_policy = 'hide'

    def __setup_toolbar(self, mdl):
        htt = [
            ("source", "$name"),
            ("index", "$index"),
            ("(x,y)", "($x, $y)")
        ]
        ht = HoverTool(tooltips=htt)
        mdl.add_tools(ht)
        mdl.add_tools(PanTool())
        mdl.add_tools(BoxZoomTool())
        mdl.add_tools(WheelZoomTool())
        mdl.add_tools(ResetTool())
        mdl.add_tools(SaveTool())
        mdl.add_tools(CrosshairTool())
        mdl.toolbar.logo = None
        mdl.toolbar.active_drag = None
        mdl.toolbar.active_scroll = None
        mdl.toolbar.active_tap = None
        mdl.toolbar.active_inspect = ht

    def __setup_figure(self, **kwargs):
        fkwargs = dict()
        if BokehVersion.base_version() > '2.4.0':
            fkwargs['output_backend'] = 'webgl'
        fkwargs['x_range'] = self._xsc.bokeh_range
        fkwargs['width'] = kwargs.get('width', 950)
        fkwargs['height'] = kwargs.get('height', 250)
        fkwargs['toolbar_location'] = 'above'
        fkwargs['tools'] = ''
        fkwargs['name'] = str(kwargs.get('uid', self.uid))
        f = figure(**fkwargs)
        f.yaxis.visible = False
        x_label = None if self._xsc is None else self._xsc.axis_label()
        if x_label is not None and x_label:
            f.xaxis.axis_label = x_label
        y_label = None if self._ysc is None else self._ysc.axis_label()
        if y_label is not None and y_label:
            f.yaxis.axis_label = y_label
        layout = kwargs.get('layout', 'column')
        if kwargs['show_title'] and layout != 'tabs':
            f.title.text = self.name if layout != 'tabs' else " "
        return f

    def __setup_glyph(self, mdl, data_source, show_legend=True, multi_y_axis=True):
        kwargs = dict()
        kwargs['x'] = self._xsn
        kwargs['y'] = data_source
        kwargs['source'] = self._cds
        kwargs['name'] = data_source
        if multi_y_axis:
            kwargs['y_range_name'] = data_source
        kwargs['line_color'] = ModelHelper.line_color(len(self._rdr))
        kwargs['y_range_name'] = data_source
        if show_legend:
            kwargs['legend_label'] = data_source + ' '
        glyph =  mdl.line(**kwargs)
        return glyph
    
    @tracer
    def setup_model(self, **kwargs):
        try:
            """asks the channel to setup then return its Bokeh associated model - returns None if no model"""
            self._mdl = None
            props = self._merge_properties(self.model_properties, kwargs)
            # x scale parameters
            self._xsc = props.get('x_scale', Scale())
            self._xsc.validate()
            # y scale parameters
            self._ysc = props.get('y_scale', Scale())
            self._ysc.validate()
            # if specified, x_channel must be one of our children
            self._xsn = self.__validate_x_channel()
            # instanciate the ColumnDataSource
            self._cds = self.__setup_cds()
            # setup figure
            show_title = True if len(self.data_sources) == 1 else False
            show_title = props.get('show_title', show_title)
            props['show_title'] = show_title
            f = self.__setup_figure(**props)
            # setup glyphs
            show_legend = False if len(self.data_sources) == 1 else True
            show_legend = props.get('show_legend', show_legend)
            i = 0
            y_range = props.get('y_range', None)
            if not isinstance(y_range, dict):
                y_range = {}
            y_axis_type = kwargs.get('y_axis_type', None)
            if not isinstance(y_axis_type, dict):
                y_axis_type = {}
            for data_source in self.data_sources:
                if data_source != self._xsn:
                    ud_axis_type = y_axis_type.get(data_source, "linear") 
                    if ud_axis_type == "log":
                        f.add_layout(LogAxis(y_range_name=data_source, axis_label=data_source), "left" if not i%2 else "right")
                    else:
                        f.add_layout(LinearAxis(y_range_name=data_source, axis_label=data_source), "left" if not i%2 else "right")
                    ud_range = y_range.get(data_source, None)
                    ds_range = DataRange1d() if ud_range is None else DataRange1d(start=ud_range[0], end=ud_range[1]) 
                    f.extra_y_ranges[data_source] = ds_range
                    self._rdr[data_source] = self.__setup_glyph(f, data_source, show_legend)
                    f.extra_y_ranges[data_source].renderers = [self._rdr[data_source]]
                    i += 1
            if show_legend:
                self.__setup_legend(f)
            # show tmp label
            self._show_msg_label(f)
            # setup the toolbar
            self.__setup_toolbar(f)
            # store figure
            self._mdl = f 
        except Exception as e:
            module_logger.exception("Exception caught!")
        return self._mdl
    
    def show_healthy_data_source(self, data_source, data_source_was_healthy):
        try:
            if not data_source_was_healthy:
                self._rdr[data_source].visible = True
                self._bad_data_source_cnt -= 1
            self.set_data_source_health(data_source, True)
        except Exception as e:
            module_logger.exception("SpectrumChannel.show_healthy_data_source failed!")

    def hide_broken_data_source(self, data_source, data_source_was_healthy):
        try:
            if data_source_was_healthy:
                self._rdr[data_source].visible = False
                self._bad_data_source_cnt += 1
            self.set_data_source_health(data_source, False)
        except Exception as e:
            module_logger.exception("SpectrumChannel.hide_broken_data_source failed!")
            
    def update(self):
        """gives each Channel a chance to update itself (e.g. to update the ColumDataSources)"""
        # print('spectrum channel update')
        if not self._mdl:
            return
        try:
            # get data from each channel
            max_len = 0
            dsd = dict()
            previous_bad_data_source_cnt = self._bad_data_source_cnt
            x_scale_data_source_broken = False
            for cn, ci in self.data_sources.items():
                ds_was_healthy = self.is_data_source_healthy(cn)
                # print("pulling data from {}...".format(sn))
                cd = ci.pull_data()
                if cd is None:
                    # no new data from data source, jump to next one
                    continue
                if cd.has_error or not cd.has_valid_data:
                    x_scale_data_source_broken = self._xsc.type == ScaleType.DATA_SOURCE and cn == self._xsn
                    self.hide_broken_data_source(cn, ds_was_healthy) 
                    self._animate_msg_label()
                    self.emit_error(cd)
                else:
                    self.show_healthy_data_source(cn, ds_was_healthy) 
                    max_len = max(max_len, cd.data[0].shape[0])
                    self._hide_msg_label()
                    dsd[cn] = cd
            if not self._bad_data_source_cnt and previous_bad_data_source_cnt:
                self.emit_recover()
            updated_data = dict()
            if x_scale_data_source_broken:
                max_len = 3
                updated_data[self._xsn] = np.linspace(-1, 1, max_len)
                self._mdl.x_range.update(start=-1, end=1)
            elif self._xsc.type == ScaleType.INDEXES:
                updated_data[self._xsn] = np.linspace(0, max_len - 1, max_len)
                self._mdl.x_range.update(start=0, end=max_len - 1)
            elif self._xsc.type == ScaleType.RANGE:
                end_point = self._xsc.start + (max_len - 1) * self._xsc.step
                updated_data[self._xsn] = np.linspace(self._xsc.start, end_point, max_len)
                self._mdl.x_range.update(start=self._xsc.start, end=end_point)
            elif self._xsc.type == ScaleType.DATA_SOURCE:
                try:
                    x_scale_data = dsd[self._xsn].data[0] 
                    x_scale_data_len = x_scale_data.shape[0]
                    max_len = max(max_len, x_scale_data_len)
                    if x_scale_data_len < max_len:
                        max_len = x_scale_data_len
                    updated_data[self._xsn] = x_scale_data
                    self._mdl.x_range.update(start=x_scale_data[0], end=x_scale_data[max_len - 1])
                except Exception:
                    updated_data[self._xsn] = np.zeros((max_len,), np.float)
                    self._mdl.x_range.update(start=0, end=0)
            else:
                raise Exception("unexpected/unsupported Scale type")
            for cn, cd in dsd.items():
                try:
                    if cn != self._xsn:
                        data = cd.data[0]
                        data_len = data.shape[0]
                        if data_len > max_len:
                            module_logger.debug("SpectrumChannel.update:shrinking data from {}".format(cn))
                            data = data[-max_len:]
                        elif data_len < max_len:
                            module_logger.debug("SpectrumChannel.update:padding data from {}".format(cn))
                            module_logger.debug("SpectrumChannel.update:padding from {} to {}".format(data_len, max_len))
                            data = np.append(data, [np.nan] * (max_len - data_len))
                            module_logger.debug("SpectrumChannel.update:padded to {}".format(data.shape))
                        updated_data[cn] = data
                        self._rdr[cn].visible = True
                except Exception:
                    updated_data[cn] = np.zeros((max_len,), np.float)
                    module_logger.exception("SpectrumChannel.update: exception caught while manipulating the data from {}".format(cn))
                    
            self._cds.data.update(updated_data)
        except Exception as e:
            module_logger.exception("SpectrumChannel.update failed!")

    def cleanup(self):
        self.__reinitialize()
        super(SpectrumChannel, self).cleanup()
        
# ------------------------------------------------------------------------------
class ImageChannel(Channel):
    """image data source channel"""

    def __init__(self, name, data_source=None, model_properties=None, image_options=None):
        Channel.__init__(self, name, data_sources=[data_source], model_properties=model_properties)
        self.__reinitialize(image_options)

    def __default_options(self):
        opts = dict()
        opts['upsidedown'] = False
        return opts 
        
    def __reinitialize(self, image_options=None):
        self._cds = None  # column data source
        self._mdl = None  # model
        self._xsc = None  # x scale
        self._ysc = None  # y scale
        self._ird = None  # image renderer
        self._itm = InteractionsManager()  # an InteractionsManager
        self._expected_image_shape = None
        self._current_image_shape = None
        self._images_size_threshold = 4000000 # 2k x 2k pixels
        self._options = self._merge_properties(image_options, self.__default_options())

    def __setup_cds(self):
        columns = dict()
        data = np.empty((2, 2))
        data.fill(np.nan)
        columns['image'] = [data]
        columns['image_width'] = [0]
        columns['image_height'] = [0]
        columns['x_hover'] = [0]
        columns['y_hover'] = [0]
        columns['z_hover'] = [0]
        columns['initial_x_range'] = [[-1, 1]]
        columns['initial_y_range'] = [[-1, 1]]
        columns['image_shape_changed'] = [0]
        return ColumnDataSource(data=columns)

    def __setup_toolbar(self, figure, w=0, h=0):
        htt = [('x','$x'), ('y','$y'), ('z','@image')]
        ht = HoverTool(tooltips=htt)
        figure.add_tools(ht)
        figure.add_tools(BoxZoomTool())
        figure.add_tools(ResetTool())
        figure.add_tools(SaveTool())
        figure.add_tools(CrosshairTool())
        figure.toolbar.logo = None
        figure.toolbar.active_drag = None
        figure.toolbar.active_scroll = None
        figure.toolbar.active_tap = None
        figure.toolbar_location = 'right' if w >= h else 'above'
        figure.toolbar.active_inspect = ht

    def get_model(self):
        """returns the Bokeh model (figure, layout, ...) associated with the Channel or None if no model"""
        return self._mdl

    @tracer
    def setup_model(self, **kwargs):
        """asks the channel to setup then return its Bokeh associated model - returns None if no model"""
        try:
            self._mdl = None
            props = self._merge_properties(self.model_properties, kwargs)
            self._cds = self.__setup_cds()
            self._xsc = props.get('x_scale', Scale())
            self._xsc.validate()
            self._ysc = props.get('y_scale', Scale())
            self._ysc.validate()
            self._images_size_threshold = self.model_properties.get('images_size_threshold', self._images_size_threshold)
            # print('ImageChannel.setup_model.images_size_threshold: {:.00f}'.format(self._images_size_threshold))
            self._expected_image_shape = self.model_properties.get('full_frame_shape', self._expected_image_shape)
            # print('ImageChannel.setup_model.expected_image_shape: {}'.format(self._expected_image_shape))
            # self.__setup_undefined_scales(self._expected_image_shape) #TODO: add an option for that
            fkwargs = dict()
            fkwargs['name'] = str(kwargs.get('uid', self.uid))
            if BokehVersion.base_version() > '2.4.0':
                fkwargs['output_backend'] = 'webgl'
            xrg = Range1d()  # self._xsc.bokeh_range
            yrg = Range1d()  # self._ysc.bokeh_range
            # print("ImageChannel.{}:set initial x-range to ({:.04f}, {:.04f})".format(self.name, xrg.start, xrg.end))
            # print("ImageChannel.{}:set initial y-range to ({:.04f}, {:.04f})".format(self.name, yrg.start, yrg.end))
            fkwargs['x_range'] = xrg
            fkwargs['y_range'] = yrg
            fkwargs['width'] = props.get('width', 320)
            fkwargs['height'] = props.get('height', 320)
            fkwargs['tools'] = ""
            f = figure(**fkwargs)
            self._cds.tags = [f.ref['id']]
            f.xaxis.axis_label = None if not self._xsc else self._xsc.axis_label()
            f.yaxis.axis_label = None if not self._ysc else self._ysc.axis_label()
            layout = kwargs.get('layout', 'column')
            if layout != 'tabs':
                f.title.text = self.name
            ikwargs = dict()
            ikwargs['x'] = 0
            ikwargs['y'] = 1
            ikwargs['dw'] = 1
            ikwargs['dh'] = 1
            ikwargs['image'] = 'image'
            ikwargs['source'] = self._cds
            ikwargs['color_mapper'] = LinearColorMapper(palette=props.get('palette', Plasma256))
            self._ird = f.image(**ikwargs)
            f.xgrid.grid_line_color = None
            f.ygrid.grid_line_color = None
            self._show_msg_label(f)
            self.__setup_toolbar(f, fkwargs['width'], fkwargs['height'])
            self._mdl = f
            bsm = props.get('selection_manager', None)
            if bsm is not None:
                bsm.attach_to_figure(f)
            self._itm.setup(self.bokeh_session, self._mdl, self.__handle_range_change, self.__handle_reset)
        except Exception as e:
            module_logger.exception("Exception caught!")
        return self._mdl

    def __setup_undefined_scales(self, img_shape):
        # print("__setup_undefined_scales.img_shape: {}".format(img_shape))
        if img_shape is None:
            return
        # print("__setup_undefined_scales._xsc.has_valid_scale: {}".format(self._xsc.has_valid_scale()))
        if not self._xsc.has_valid_scale():
            skwargs = dict()
            skwargs['start'] = 0
            skwargs['end'] = img_shape[1] - 1
            skwargs['num_points'] = img_shape[1]
            self._xsc = Scale(**skwargs)
        # print("__setup_undefined_scales._ysc.has_valid_scale: {}".format(self._ysc.has_valid_scale()))
        if not self._ysc.has_valid_scale():
            skwargs = dict()
            skwargs['start'] = 0 if not self._options['upsidedown'] else img_shape[0] - 1
            skwargs['end'] = img_shape[0] - 1 if not self._options['upsidedown'] else 0
            skwargs['num_points'] = img_shape[0]
            self._ysc = Scale(**skwargs)

    def __handle_range_change(self):
        try:
            sd = self._sd
            if not sd or sd.has_error or not sum(sd.data[0].shape):
                return
            # print("ImageChannel.{}:handle_range_change: x-range changed to ({:.04f}, {:.04f})".format(self.name, self._mdl.x_range.start, self._mdl.x_range.end))
            # print("ImageChannel.{}:handle_range_change: y-range changed to ({:.04f}, {:.04f})".format(self.name, self._mdl.y_range.start, self._mdl.y_range.end))
            image = self.__extract_image_for_current_ranges(sd.data[0])
            new_data = dict()
            new_data['image'] = [image]
            new_data['image_width'] = [image.shape[1]]
            new_data['image_height'] = [image.shape[0]]
            self._cds.data.update(new_data)
            self._ird.glyph.update(x=self._mdl.x_range.start,
                                   y=self._mdl.y_range.start,
                                   dw=abs(self._mdl.x_range.end - self._mdl.x_range.start),
                                   dh=abs(self._mdl.y_range.end - self._mdl.y_range.start))
        except Exception as e:
            module_logger.exception("Exception caught!")
        finally:
            self._itm.range_change_handled()

    def __handle_reset(self):
        try:
            module_logger.info("ImageChannel.{}:__handle_reset called!".format(self.name))
            self._mdl.x_range.start = self._xsc.start
            self._mdl.x_range.end = self._xsc.end
            self._mdl.y_range.start = self._ysc.start
            self._mdl.y_range.end = self._ysc.end
            self.__handle_range_change()
        except Exception as e:
            module_logger.exception("Exception caught!")
        finally:
            pass

    def __image_shape_changed(self, image_shape):
        return self._current_image_shape != image_shape

    def __extract_image_for_current_ranges(self, image):
        xsc = self._mdl.x_range.start
        xec = self._mdl.x_range.end
        xx = np.linspace(self._xsc.start, self._xsc.end, num=image.shape[1], dtype=float)
        xy = np.linspace(0, image.shape[1] - 1, num=image.shape[1], dtype=int)
        ysc = self._mdl.y_range.start
        yec = self._mdl.y_range.end
        yis = self._ysc.start 
        yie = self._ysc.end
        if self._options['upsidedown']:
            # print("upsidedown enabled: swapping y scale limits")
            ysc, yec = yec, ysc
            yis, yie = yie, yis
        # print("extract_image_for_current_ranges: x:({:.04f}, {:.04f}) - y:({:.04f} -> {:.04f})".format(xsc, xec, ysc, yec))
        yx = np.linspace(yis, yie, num=image.shape[0], dtype=float)
        yy = np.linspace(0, image.shape[0] - 1, num=image.shape[0], dtype=int)
        xsi = int(mt.floor(np.interp(xsc, xx, xy)))
        xei = int(mt.ceil(np.interp(xec, xx, xy)) + 1)
        ysi = int(mt.floor(np.interp(ysc, yx, yy)))
        yei = int(mt.ceil(np.interp(yec, yx, yy)) + 1)
        # print("extract_image_for_current_ranges: x:[{}, {}] - y:[{}, {}]".format(xsi, xei, ysi, yei))
        image = image[ysi:yei, xsi:xei]
        image = image if not self._options['upsidedown'] else image[::-1]
        # print("shape of extracted image from buffer {}".format(image.shape))
        # print("extract_image_for_current_ranges.sub_image.shape: {}".format(image.shape))
        need_rescale, rescaling_factor = (False, 0) if rescaler is not None else self.__compute_rescaling_factor(image)
        if need_rescale:
            image = self.__rescale_image(image, rescaling_factor)
            # print("extract_image_for_current_ranges.sub_image.rescaled to {}".format(image.shape))
        return image

    def __compute_rescaling_factor(self, image):
        try:
            rescaling_factor = 1.0
            initial_image_size = image_size = image.shape[0] * image.shape[1]
            # print("compute_rescaling_factor: size: {:.04f} - threshold: {:.04f}".format(image_size, image_size_threshold))
            if image_size <= self._images_size_threshold:
                # print("compute_rescaling_factor: no rescaling required")
                return False, rescaling_factor
            for inc in [0.1, 0.01, 0.001, 0.0001]:
                while image_size > self._images_size_threshold:
                    rescaling_factor -= inc
                    image_size = int(initial_image_size * rescaling_factor)
                rescaling_factor += inc
                image_size = int(initial_image_size * rescaling_factor)
            rescaling_factor = mt.sqrt(rescaling_factor)
            # print("compute_rescaling_factor.rescaling factor: {:.04f}".format(rescaling_factor))
            return True, rescaling_factor
        except Exception as e:
            module_logger.exception("Exception caught!")

    def __rescale_image(self, in_img, rescaling_factor):
        # print('rescale-image: in shape {}'.format(in_img.shape))
        if has_opencv:
            width = int(in_img.shape[1] * rescaling_factor)
            height = int(in_img.shape[0] * rescaling_factor)
            out_img = rescaler(in_img, (width, height), interpolation=cv2.INTER_CUBIC)
        elif has_skimage:
            out_img = rescaler(in_img, rescaling_factor, mode='constant', cval=np.nan)
        else:
            out_img = in_img
        # print('rescale-image: out shape {}'.format(out_img.shape))
        return out_img

    def update(self, update_image=True):
        """gives each Channel a chance to update itself (e.g. to update the ColumnDataSources)"""
        try:
            ds = self.data_source
            if ds is None:
                return
            if update_image:
                self._sd = ds.pull_data()
            if self._sd is None:
                return
            sd = self._sd
            previous_bad_data_source_cnt = self._bad_data_source_cnt
            if sd.has_error:
                self._bad_data_source_cnt = 1
                module_logger.info("ImageChannel: emitting error...")
                self.emit_error(sd)
            elif previous_bad_data_source_cnt:
                self._bad_data_source_cnt = 0
                module_logger.info("ImageChannel: emitting recover...") 
                self.emit_recover()
            empty_buffer = sd.has_error or not all(sd.data[0].shape)
            nan_buffer = None
            if empty_buffer:
                nan_buffer = np.empty((2, 2))
                nan_buffer.fill(np.nan)
                self._animate_msg_label()
            else:
                self._hide_msg_label()
            incoming_image = sd.data[0] if not empty_buffer else nan_buffer
            image_shape_changed = self.__image_shape_changed(incoming_image.shape)
            self._current_image_shape = incoming_image.shape
            if not empty_buffer:
                self.__setup_undefined_scales(sd.data[0].shape)
            if empty_buffer:
                xss = -1.
                xse = 1.
                xst = 1.
                xpn = 3
            elif self._xsc.type != ScaleType.INDEXES:
                xss = self._xsc.start
                if self._expected_image_shape is None:
                    xse = self._xsc.start + ((sd.data[0].shape[1] - 1) * self._xsc.step)
                else:
                    xse = self._xsc.end
                xst = self._xsc.step
                xpn = sd.data[0].shape[1]
            else:
                xss = 0.
                xse = sd.data[0].shape[1]
                xst = 1.
                xpn = sd.data[0].shape[1]
            if empty_buffer:
                yss = -1.
                yse = 1.
                yst = 1.
                ypn = 3
            elif self._ysc.type != ScaleType.INDEXES:
                yss = self._ysc.start
                if self._expected_image_shape is None:
                    yse = self._ysc.start + ((sd.data[0].shape[0] - 1) * self._ysc.step)
                else:
                    yse = self._ysc.end
                yst = self._ysc.step
                ypn = sd.data[0].shape[0]
            else:
                yss = 0. if not self._options['upsidedown'] else sd.data[0].shape[0]
                yse = sd.data[0].shape[0] if not self._options['upsidedown'] else 0
                yst = 1.
                ypn = sd.data[0].shape[0]
            w = abs(xse - xss)
            h = abs(yse - yss)
            if not w:
                xss = -1.
                xse = 1.
            if not h:
                yss = -1.
                yse = 1.
            sign = 1. if not self._options['upsidedown'] else -1.
            if image_shape_changed and not empty_buffer:
                # print("ImageChannel.update.{}:changing x-range to ({:.04f}, {:.04f})".format(self.name, xss, xse))
                # print("ImageChannel.update.{}:changing y-range to ({:.04f}, {:.04f})".format(self.name, yss, yse))
                self._mdl.x_range.update(start=xss, end=xse)
                self._mdl.y_range.update(start=yss, end=yse)
                self._ird.glyph.update(x=xss, y=yss, dw=w, dh=h)
            else:
                x = self._mdl.x_range.start
                y = self._mdl.y_range.start
                dw = abs(self._mdl.x_range.end - self._mdl.x_range.start)
                dh = abs(self._mdl.y_range.end - self._mdl.y_range.start)
                self._ird.glyph.update(x=x, y=y, dw=dw, dh=dh)
            if not empty_buffer:
                image = self.__extract_image_for_current_ranges(incoming_image)
            else:
                image = nan_buffer
            new_data = dict()
            new_data['image'] = [image]
            new_data['image_width'] = [image.shape[1]]
            new_data['image_height'] = [image.shape[0]]
            if image_shape_changed:
                new_data['image_shape_changed'] = [1]
                new_data['initial_x_range'] = [[xss, xse]]
                new_data['initial_y_range'] = [[yss, yse]]
            else:
                new_data['image_shape_changed'] = [0]
            self._cds.data.update(new_data)
        except Exception as e:
            module_logger.exception("Exception caught!")

    def cleanup(self):
        self.__reinitialize()
        super(ImageChannel, self).cleanup()


# ------------------------------------------------------------------------------
class GenericChannel(Channel):
    """this is not supposed to be instanciated directly"""

    def __init__(self, name, data_source=None, model_properties=dict()):
        Channel.__init__(self, name, data_sources=[data_source], model_properties=model_properties)
        self._delegate = None
        self._delegate_model_id = str(uuid4().hex)
        self._delegate_model = None

    def get_model(self):
        """returns the Bokeh model (figure, layout, ...) associated with the Channel or None if no model"""
        return self._delegate_model

    @tracer
    def setup_model(self, **kwargs):
        """asks the channel to setup then return its Bokeh associated model - returns None if no model"""
        try:
            self.update()
        finally:
            return self._delegate_model

    def update(self):
        """gives each Channel a chance to update itself (e.g. to update the ColumDataSources)"""
        try:
            if self._delegate:
                self._delegate.update()
                return
            ds = self.data_source
            if ds is None:
                return
            sd = ds.pull_data()
            if sd is None:
                return 
            previous_bad_data_source_cnt = self._bad_data_source_cnt
            if sd.has_error:
                self._bad_data_source_cnt = 1
                self.emit_error(sd)
                return
            elif previous_bad_data_source_cnt:
                self._bad_data_source_cnt = 0
                self.emit_recover()
            self.model_properties['uid'] = self.uid
            if sd.format == ChannelData.Format.SCALAR:
                #module_logger.debug("GenericChannel.update.instanciating SCALAR channel")
                self._delegate = ScalarChannel(name=self.name,
                                               data_sources=[self.data_source],
                                               model_properties=self.model_properties)
            elif sd.format == ChannelData.Format.SPECTRUM:
                #module_logger.debug("GenericChannel.update.instanciating SPECTRUM channel")
                self._delegate = SpectrumChannel(name=self.name,
                                                 data_sources=[self.data_source],
                                                 model_properties=self.model_properties)
            elif sd.format == ChannelData.Format.IMAGE:
                #module_logger.debug("GenericChannel.update.instanciating IMAGE channel")
                self._delegate = ImageChannel(name=self.name,
                                              data_source=self.data_source,
                                              model_properties=self.model_properties,
                                              image_options=self.model_properties)
            if self._delegate:
                self._delegate.output = self.output
                events = [DataStreamEvent.Type.ERROR, DataStreamEvent.Type.RECOVER, DataStreamEvent.Type.EOS]
                self._delegate.register_event_handler(self, events)
                self._delegate.bokeh_session = self.bokeh_session
                self._delegate_model = self._delegate.setup_model()
                self.emit_model_changed(self._delegate_model)
        except Exception as e:
            module_logger.exception("Exception caught!")

    def cleanup(self):
        self._delegate = None
        super(GenericChannel, self).cleanup()


# ------------------------------------------------------------------------------
class LayoutChannel(Channel):
    """simulates a data monitor handling several image data sources"""

    def __init__(self, name, channels=None, model_properties=None):
        Channel.__init__(self, name, model_properties=model_properties)
        # model
        self._mdl = None
        # tabs widget (layout option)
        self._tabs_widget = None
        # sub-channels
        self._channels = Children(self, Channel)
        self._channels.register_add_callback(self.__on_add_channel)
        self.add(channels)

    @Channel.bokeh_session.setter
    def bokeh_session(self, bks):
        """overwrites Channel.bokeh_session.setter"""
        assert (isinstance(bks, BokehSession))
        self._session = bks
        for channel in self._channels.values():
            channel.bokeh_session = bks

    @NotebookCellContent.output.setter
    def output(self, new_output):
        """overwrites NotebookCellContent.output.setter"""
        self._output = new_output
        for channel in self._channels.values():
            channel.output = new_output

    def add(self, channels):
        """add the specified (sub)channels"""
        self._channels.add(channels)

    def __on_add_channel(self, channel):
        """called when a sub-channel is added to this channel"""
        if channel is not self:
            channel.output = self.output
            events = [DataStreamEvent.Type.ERROR, DataStreamEvent.Type.RECOVER, DataStreamEvent.Type.MODEL_CHANGED]
            channel.register_event_handler(self, events)

    @property
    def model(self):
        """returns the Bokeh model (figure, layout, ...) associated with the Channel or None if no model"""
        return self._mdl if self._mdl else self.setup_model()

    @classmethod
    def __model_width(cls, model):
        if hasattr(model, 'plot_width'):
            # print("model_width:plot_width={}".format(model.plot_width))
            w = model.plot_width
        elif hasattr(model, 'width'):
            # print("model_width:width={}".format(model.width))
            w = model.width
        else:
            # print("model_width:width=MAXSIZE={}".format(2**32))
            w = 2**32
        return w

    @classmethod
    def __model_height(cls, model):
        if hasattr(model, 'plot_height'):
            # print("model_width:plot_height={}".format(model.plot_height))
            h = model.plot_height
        elif hasattr(model, 'height'):
            # print("model_width:height={}".format(model.height))
            h = model.height
        else:
            # print("model_width:height=MAXSIZE={}".format(2**32))
            h = 2**32
        return h

    def __setup_layout(self, children, **kwargs):
        """setup the layout"""
        layout = kwargs.get('layout', 'column')
        if layout == 'tabs':
            return self.__setup_tabs_layout(children, **kwargs)
        elif layout == 'grid':
            return self.__setup_grid_layout(children, **kwargs)
        elif layout == 'row':
            return self.__setup_row_layout(children, **kwargs)
        else:
            return self.__setup_column_layout(children, **kwargs)

    def __setup_grid_layout(self, children, **kwargs):
        """spread the children in rows"""
        try:
            if len(children) > 1:
                num_columns, width_sum = 1, 0
                # print("setup_grid_layout: images layout contains {} children".format(len(children)))
                for c in children.values():
                    width_sum += self.__model_width(c)
                    if width_sum <= 800: #TODO: smarter impl required
                        num_columns += 1
                    else:
                        break
                num_rows = int(ceil(float(len(children)) / float(num_columns)))
                # print("num_columns={} - num_rows={}".format(num_columns, num_rows))
                rl = list()
                for i in range(num_rows):
                    rl.append([None for i in range(num_columns)])
                ri, fi = 0, 0
                for c in children.values():
                    rl[ri][fi] = c
                    fi = (fi + 1) % num_columns
                    if not fi:
                        ri += 1
                tbo = dict()
                tbo['logo'] = None
                merge_tools = kwargs.get('merge_tools', True)
                return gridplot(children=rl, toolbar_location='above', toolbar_options=tbo, merge_tools=merge_tools, sizing_mode='stretch_both')
            else:
                return layout(children=children.values(), sizing_mode='stretch_both')
        except Exception as e:
            raise

    def __setup_column_layout(self, children, **kwargs):
        """spread the children in rows"""
        ml = list()
        for c in children.values():
            ml.append(c)
        return column(name=str(self.uid), children=ml, sizing_mode='stretch_both')
    
    def __setup_row_layout(self, children, **kwargs):
        """spread the children in rows"""
        ml = list()
        for c in children.values():
            ml.append(c)
        return row(name=str(self.uid), children=ml, sizing_mode='stretch_both')
    
    def __setup_tabs_layout(self, children, **kwargs):
        """spread the children in tabs"""
        tl = list()
        for cn, ci in children.items():
            tl.append(BokehWidgets.Panel(child=ci, title=cn))
        self._tabs_widget = BokehWidgets.Tabs(tabs=tl)
        self._tabs_widget.on_change("active", self.on_tabs_selection_change)
        return gridplot([[column(name=str(self.uid), children=[self._tabs_widget])]], sizing_mode='stretch_both') 

    def get_model(self):
        """returns the Bokeh model (figure, layout, ...) associated with the Channel or None if no model"""
        return self._mdl

    @tracer
    def setup_model(self, **kwargs):
        """asks the channel to setup then return its Bokeh associated model - returns None if no model"""
        try:
            self._mdl = None
            props = self._merge_properties(self.model_properties, kwargs)
            cd = OrderedDict()
            for cn, ci in self._channels.items():
                cd[cn] = ci.setup_model(**props)
            self._mdl = self.__setup_layout(cd, **props)
        except Exception as e:
            module_logger.exception("Exception caught!")
        return self._mdl

    def on_tabs_selection_change(self, attr, old, new):
        self.__update_tabs_selection()

    def __update_tabs_selection(self):
        # TODO: we might face a race condition between server periodic callback and user action: mutex required?
        at = self._tabs_widget.active
        cn = self._tabs_widget.tabs[at].title
        self._channels[cn].update()

    def update(self):
        try:
            if self._tabs_widget:
                self.__update_tabs_selection()
            else:
                for c in self._channels.values():
                    c.update()
        except Exception as e:
            module_logger.exception("Exception caught!")


# ------------------------------------------------------------------------------
class DataStream(NotebookCellContent, DataStreamEventHandler):
    """data stream interface"""

    def __init__(self, name, channels=None):
        NotebookCellContent.__init__(self, name, logger=module_logger)
        DataStreamEventHandler.__init__(self, name)
        # bokeh session
        self._session = None
        # channels
        self._channels = Children(self, Channel)
        self._channels.register_add_callback(self._on_add_channel)
        self.add(channels)

    @property
    def bokeh_session(self):
        """return the associated bokeh session"""
        return self._session

    @bokeh_session.setter
    def bokeh_session(self, bks):
        """set the associated bokeh session"""
        assert (isinstance(bks, BokehSession))
        self._session = bks
        for channel in self._channels.values():
            channel.bokeh_session = bks

    @NotebookCellContent.output.setter
    def output(self, new_output):
        """overwrites NotebookCellContent.output.setter"""
        self._output = new_output
        for channel in self._channels.values():
            channel.output = new_output

    def add(self, channels):
        """add the specified channels"""
        self._channels.add(channels)

    def _on_add_channel(self, channel):
        """called when a new channel is added to the data stream"""
        channel.output = self.output
        events = [DataStreamEvent.Type.ERROR, DataStreamEvent.Type.RECOVER, DataStreamEvent.Type.EOS]
        channel.register_event_handler(self, events)

    def get_models(self):
        """returns the Bokeh model (figure, layout, ...)s associated with the DataStream"""
        return [channel.get_model() for channel in self._channels.values()]

    def setup_models(self):
        """returns the Bokeh model (figure, layout, ...)s associated with the DataStream"""
        models = list()
        for channel in self._channels.values():
            model = channel.setup_model()
            if model:
                models.append(model)
        return models

    def refresh_period_changed(self, up):
        for channel in self._channels.values():
            try:
                module_logger.debug("DataStream.refresh_period_changed for Channel {}".format(channel.name))
                channel.refresh_period_changed(up)
            except Exception as e:
                module_logger.exception("Exception caught!")
           
    def history_buffer_depth_changed(self, hbd):
        for channel in self._channels.values():
            try:
                module_logger.debug("DataStream.history_buffer_depth_changed for Channel {}".format(channel.name))
                channel.history_buffer_depth_changed(hbd)
            except Exception as e:
                module_logger.exception("Exception caught!")
            
    def update(self):
        """gives each Channel a chance to update itself (e.g. to update the ColumDataSources)"""
        # print("data stream: {} update".format(self.name))
        for channel in self._channels.values():
            try:
                channel.update()
            except Exception as e:
                module_logger.exception("Exception caught!")

    def reset(self):
        """asks each Channel to reset itself"""
        for channel in self._channels.values():
            try:
                module_logger.debug("DataStream: resetting Channel {}".format(channel.name))
                channel.reset()
            except Exception as e:
                module_logger.exception("Exception caught!")
                
    def cleanup(self):
        """asks each Channel to cleanup itself (e.g. release resources)"""
        for channel in self._channels.values():
            try:
                module_logger.debug("DataStream: cleaning up Channel {}".format(channel.name))
                channel.cleanup()
            except Exception as e:
                module_logger.exception("Exception caught!")

                
# ------------------------------------------------------------------------------
class DataStreamer(NotebookCellContent, DataStreamEventHandler, BokehSession):
    """a data stream manager embedded a bokeh server"""

    def __init__(self, name, data_streams, **kwargs):
        NotebookCellContent.__init__(self, name, logger=module_logger, output=kwargs.get('output', None))
        DataStreamEventHandler.__init__(self, name)
        BokehSession.__init__(self)
        # a FIFO to store incoming DataStreamEvent
        self._events = deque()
        # update period in seconds
        self.callback_period = kwargs.get('refresh_period', 1.)
        # the data streams
        self._data_streams = list()
        self.add(data_streams)
        # auto start
        self._auto_start = kwargs.get('auto_start', False)
        # start delay
        self._start_delay = kwargs.get('start_delay', 0.)
        # open the session
        if self._auto_start:
            self.open()

    @property
    def bokeh_session(self):
        """return the associated bokeh session"""
        return self

    @NotebookCellContent.output.setter
    def output(self, new_output):
        """overwrites NotebookCellContent.output.setter"""
        self._output = new_output
        for ds in self._data_streams:
            ds.output = new_output
    
    def add(self, ds):
        events = [DataStreamEvent.Type.ERROR, DataStreamEvent.Type.RECOVER, DataStreamEvent.Type.MODEL_CHANGED]
        if isinstance(ds, DataStream):
            ds.bokeh_session = self
            ds.output = self.output
            ds.register_event_handler(self, events)
            self._data_streams.append(ds)
        elif isinstance(ds, (list, tuple)):
            for s in ds:
                if not isinstance(s, DataStream):
                    raise ValueError("invalid argument: expected a list, a tuple or a single instance of DataStream")
                s.bokeh_session = self
                s.output = self.output
                s.register_event_handler(self, events)
                self._data_streams.append(s)
        else:
            raise ValueError("invalid argument: expected a list, a tuple or a single instance of DataStream")

    def remove(self, ds):
        if isinstance(ds, (str, bytes)):
            dsi = self.get_stream(ds)
            if dsi is not None:
                dsi.cleanup()
                self._data_streams.remove(dsi)
        elif isinstance(ds, DataStream):
            ds.cleanup()
            self._data_streams.remove(ds)
        elif isinstance(ds, (list, tuple)):
            for s in ds:
                if not isinstance(s, DataStream):
                    raise ValueError("invalid argument: expected a list, a tuple or a single instance of DataStream")
                s.cleanup()
                self._data_streams.remove(s)
        else:
            raise ValueError("invalid argument: expected a list, a tuple or a single instance of DataStream")
    
    def get_stream(self, stream_name):
        for ds in self._data_streams:
            if ds.name == stream_name:
                return ds
        return None
            
    @tracer
    def open(self):
        """open the session and optionally start it """
        with self.output:
            super(DataStreamer, self).open()

    @tracer
    def close(self):
        """close the session"""
        # suspend periodic callback
        self.pause()
        # the underlying actions will be performed under critical section
        self.safe_document_modifications(self.__close)
        
    @tracer
    def __close(self):
        """close/cleanup everything"""
        # cleanup each data stream
        for ds in self._data_streams:
            try:
                module_logger.debug("DataStreamer: cleaning up DataStream {}".format(ds.name))
                ds.cleanup()
            except Exception as e:
                module_logger.exception("Exception caught!")
        module_logger.debug("DataStreamer: closing Bokeh session...")
        # delegate the remaining actions to our super class (this is mandatory)
        try:
            super(DataStreamer, self).cleanup(False)
            module_logger.debug("DataStreamer: Bokeh session closed")
        except Exception as e:
            module_logger.exception("Exception caught!")
        
    @tracer
    def start(self, delay=0.):
        """start periodic activity"""
        if not self.ready:
            module_logger.debug("DataStreamer.start:session not ready:set auto_start to True")
            self._auto_start = True
        else:
            actual_tmo = delay if delay > 0. else self._start_delay
            if actual_tmo > 0.:
                module_logger.debug('DataStreamer.start: actual start in {} seconds'.format(actual_tmo))
                self._start_delay = 0.
                self.timeout_callback(self.start, actual_tmo)
            else:
                module_logger.debug("DataStreamer.start: session ready, no delay, resuming...")
                self.resume()

    @tracer
    def stop(self):
        """stop periodic activity"""
        if not self.ready:
            self._auto_start = False
        else:
            self.pause()

    @tracer
    def setup_document(self):
        """add the data stream models to the bokeh document"""
        try:
            models = list()
            for ds in self._data_streams:
                try:
                    models.extend(ds.setup_models())
                except Exception as e:
                    module_logger.exception("Exception caught!")
            try:
                self.periodic_callback()
            except:
                pass
            try:
                for model in models:
                    self.document.add_root(model, setter=self.bokeh_session_id)
                if self._auto_start:
                    self.start(self._start_delay)
            except Exception as e:
                module_logger.exception("Exception caught!")
        except Exception as e:
            module_logger.exception("Exception caught!")
            raise

    def handle_stream_event(self, event):
        assert (isinstance(event, DataStreamEvent))
        if event.type == DataStreamEvent.Type.MODEL_CHANGED:
            self._events.appendleft(event)
            self.safe_document_modifications(self.__on_model_changed)

    def __on_model_changed(self):
        event = self._events.pop()
        if event.emitter and event.data:
            # print("handling DataStreamEvent.Type.MODEL_CHANGED")
            if len(self.document.roots):
                for root in self.document.roots:
                    if root.name == str(event.emitter):
                        # print("removing figure {}".format(root.name))
                        self.document.remove_root(root)
            try:
                # print("adding new root {}:{} to document".format(event.data, event.data.name))
                self.document.add_root(event.data, setter=self.bokeh_session_id)
            except Exception as e:
                module_logger.exception("Exception caught!")
                # print("DataStreamEvent.Type.MODEL_CHANGED successfully handled")

    @property
    def refresh_period(self):
        """returns the update period (in seconds)"""
        return self.callback_period

    @refresh_period.setter
    def refresh_period(self, up):
        """set the update period (in seconds)"""
        self.refresh_period_changed(up)
    
    def refresh_period_changed(self, up):
        """set the update period (in seconds)"""
        # update the update period at bokeh session level
        self.update_callback_period(up)
        # update the update period at datasources level
        for ds in self._data_streams:
            module_logger.debug("DataStreamer.refresh_period_changed for DataStream {}".format(ds.name))
            ds.refresh_period_changed(up)
     
    def history_buffer_depth_changed(self, hbd):
        """set the history length (in seconds)"""
        # update the update period at datasources level
        for ds in self._data_streams:
            module_logger.debug("DataStreamer.history_buffer_depth_changed for DataStream {}".format(ds.name))
            ds.history_buffer_depth_changed(hbd)
            
    def periodic_callback(self):
        """the session periodic callback"""
        for ds in self._data_streams:
            try:
                ds.update()
            except Exception as e:
                module_logger.exception("Exception caught!")

    def reset(self):
        """asks each Channel to reset itself"""
        for ds in self._data_streams:
            try:
                module_logger.debug("DataStreamer: resetting up DataStream {}".format(ds.name))
                ds.reset()
            except Exception as e:
                module_logger.exception("Exception caught!")
                

# ------------------------------------------------------------------------------
class DataStreamerController(NotebookCellContent, DataStreamEventHandler):
    """a DataStreamer controller"""

    def __init__(self, name, data_streamer=None, **kwargs):
        try:
            # check input parameters
            assert(isinstance(data_streamer, DataStreamer))
            # route output to current cell
            NotebookCellContent.__init__(self,
                                         name,
                                         output=kwargs.get('output', None),
                                         logger=module_logger)
            DataStreamEventHandler.__init__(self, name)
            # start/stop/close button
            self.__setup_controls(data_streamer, **kwargs)
            # data streamer
            self.data_streamer = data_streamer
            # function called when the close button is clicked
            self._close_callbacks = list()
            # auto-start
            self._running = False
            auto_start = kwargs.get('auto_start', True)
            if auto_start:
                self._data_streamer.open()
                self._data_streamer.start()
                self.__on_freeze_unfreeze_clicked()
        except Exception as e:
            module_logger.exception("Exception caught!")

    @staticmethod
    def l01a(width='auto', *args, **kwargs):
        return ipw.Layout(flex='0 1 auto', width=width, *args, **kwargs)

    @staticmethod
    def l11a(width='auto', *args, **kwargs):
        return ipw.Layout(flex='1 1 auto', width=width, *args, **kwargs)

    def __setup_refresh_period_slider(self, data_streamer, **kwargs):
        return ipw.FloatSlider(
            value=kwargs.get('refresh_period', 1.0),
            min=kwargs.get('min_refresh_period', 0.1),
            max=kwargs.get('max_refresh_period', 5.0),
            step=kwargs.get('step_refresh_period', 0.1),
            description='Update period (s)',
            disabled=False,
            continuous_update=False,
            orientation='horizontal',
            readout=True,
            readout_format='.2f',
            style={'description_width':'initial'}
        )

    # THIS HAS A HUGE PERFORMANCE COST DUE TO THE LOCKING OVERHEAD IT REQUIRES
    def __setup_history_buffer_depth_slider(self, data_streamer, **kwargs):
        min_hbd = kwargs.get('min_history_buffer_depth', 10.)
        max_hbd = kwargs.get('min_history_buffer_depth', 3600)
        history_buffer_depth = max(min_hbd, kwargs.get('history_buffer_depth', kwargs.get('history_buffer_depth', 120)))
        history_buffer_depth = min(history_buffer_depth, max_hbd)
        return ipw.IntSlider(
            value=history_buffer_depth,
            min=kwargs.get('min_history_buffer_depth', min_hbd),
            max=kwargs.get('min_history_buffer_depth', max_hbd),
            step=kwargs.get('step_history_buffer_depth', 10),
            description='History Buffer Depth (s)',
            disabled=False,
            continuous_update=False,
            orientation='horizontal',
            readout=True,
            style={'description_width':'initial'}
        )
    
    def __setup_controls(self, data_streamer, **kwargs):
        self._error_area = None
        self._error_area_enabled = kwargs.get('error_area_enabled', True)
        if self._error_area_enabled:
            self._error_area = ipw.Textarea(value="", rows=1, layout=self.l11a(), placeholder="Everything's up and running...") 
        if kwargs.get('up_slider_enabled', True):
            self._up_slider = self.__setup_refresh_period_slider(data_streamer, **kwargs)
            self._up_slider.observe(self.__on_refresh_period_changed, names=['value'])
        else:
            self._up_slider = None
        # THIS HAS A HUGE PERFORMANCE COST DUE TO THE LOCKING OVERHEAD IT REQUIRES
        # LET'S DISABLE IT - KEEP THE CODE JUST IN CASE...
        kwargs['hbd_slider_enabled'] = False
        if kwargs.get('hbd_slider_enabled', False):
            self._hbd_slider = self.__setup_history_buffer_depth_slider(data_streamer, **kwargs)
            self._hbd_slider.observe(self.__on_history_buffer_depth_changed, names=['value'])
        else:
            self._hbd_slider = None
        bd = "Freeze" if kwargs.get('auto_start', True) else "Unfreeze"
        buttons_list = list()
        self._freeze_unfreeze_button = ipw.Button(description=bd, layout=self.l01a(width="100px"))
        self._freeze_unfreeze_button.on_click(self.__on_freeze_unfreeze_clicked)
        buttons_list.append(self._freeze_unfreeze_button)
        if kwargs.get('reset_button_enabled', False):
            self._reset_button = ipw.Button(description="Reset", layout=self.l01a(width="100px"))
            self._reset_button.on_click(self.__on_reset_clicked)
            buttons_list.append(self._reset_button)
        else:
            self._reset_button = None
        self._close_button = ipw.Button(description="Close", layout=self.l01a(width="100px"))
        self._close_button.on_click(self.__on_close_clicked)
        buttons_list.append(self._close_button)
        self._switch_buttons_to_valid_state()
        widgets_list = list()
        title = kwargs.get('title', None)
        if title:
            div = ipw.HTML(value="<b>{}</b>:".format(title))
            widgets_list.append(div)
            lbl = ipw.HTML(value="", layout=self.l01a(width="40px"))
            widgets_list.append(lbl)
        if self._up_slider:
            widgets_list.append(self._up_slider)
        if self._hbd_slider:
            widgets_list.append(self._hbd_slider)
        widgets_list.extend(buttons_list)
        main_controls = ipw.HBox(widgets_list, layout=self.l01a())
        self._ea_output = ipw.Output(layout=self.l11a())
        with self._ea_output:
            display(self._error_area)
        #self._ea_output.layout.visibility = 'visible'
        #self._ea_output.layout.border = "1px solid grey"
        self._ds_output = ipw.Output(layout=self.l11a())
        #self._ds_output.layout.border = "1px solid red"
        self._controls = ipw.VBox([main_controls, self._ea_output, self._ds_output], layout=self.l01a())
        self.display(self._controls)

    def __on_refresh_period_changed(self, event):
        try:
            self.data_streamer.refresh_period = event['new']
        except Exception as e:
            module_logger.exception("Exception caught!")

    def __on_history_buffer_depth_changed(self, event):
        try:
            #print(event)
            new_value = event['new']
            #print("DataStreamerController.__on_history_buffer_depth_changed: {}".format(new_value))
            self.data_streamer.history_buffer_depth_changed(new_value)
        except Exception as e:
            module_logger.exception("Exception caught!")
            
    def __on_freeze_unfreeze_clicked(self, b=None):
        if self._running:
            self._data_streamer.stop()
            self._freeze_unfreeze_button.description = "Unfreeze"
        else:
            self._data_streamer.start()
            self._freeze_unfreeze_button.description = "Freeze"
        self._running = not self._running
        if self._running and self._up_slider is not None:
            self._up_slider.value = self.data_streamer.refresh_period

    def __on_reset_clicked(self, b=None):
        self.reset()
        
    def __on_close_clicked(self, b=None):
        self.close()

    def start(self, delay=0.):
        try:
            module_logger.debug("DataStreamerController : starting DataStreamer {}".format(self._data_streamer.name[-10:]))
            self._data_streamer.start(delay)
        except Exception as e:
            module_logger.exception("Exception caught!")

    def stop(self):
        module_logger.debug("DataStreamerController : stopping DataStreamer {}".format(self._data_streamer.name[-10:]))
        pass
    
    def reset(self):
        try:
            module_logger.debug("DataStreamerController : resetting DataStreamer {}".format(self._data_streamer.name[-10:]))
            self._data_streamer.reset()
        except Exception as e:
            module_logger.exception("Exception caught!")
        
    def close(self):
        try:
            module_logger.debug("DataStreamerController : closing DataStreamer {}".format(self._data_streamer.name[-10:]))
            self._data_streamer.close()
            self.__call_close_callbacks()
        except Exception as e:
            module_logger.exception("Exception caught!")
        self._ea_output.clear_output()
        self._ea_output.close()
        self._ds_output.clear_output()
        self._ds_output.close()
        self._controls.close()

    def register_close_callback(self, cb, args=None):
        assert (hasattr(cb, '__call__'))
        self._close_callbacks.append({'func':cb, 'args':args})

    def __call_close_callbacks(self):
        for cb in self._close_callbacks:
            try:
                if cb['args'] is not None:
                    cb['func'](cb['args'])
                else:
                     cb['func']()
            except Exception as e:
                module_logger.exception("Exception caught!")

    def handle_stream_event(self, event):
        assert (isinstance(event, DataStreamEvent))
        if event.type == DataStreamEvent.Type.ERROR:
            module_logger.info("DataStreamerController.{}: handling DataStreamEvent.Type.ERROR".format(self.name))
            self.__on_stream_error(event)
        elif event.type == DataStreamEvent.Type.RECOVER:
            module_logger.info("DataStreamerController.{}: handling DataStreamEvent.Type.RECOVER".format(self.name))
            self.__on_stream_recover(event)
        elif event.type == DataStreamEvent.Type.EOS:
            module_logger.info("DataStreamerController.{}: handling DataStreamEvent.Type.EOS".format(self.name))
            self.__on_end_of_stream(event)

    def __on_stream_error(self, event):
        self._switch_buttons_to_invalid_state()
        self._show_error(event.error)

    def __on_stream_recover(self, event):
        self._switch_buttons_to_valid_state()
        self._hide_error()

    def __on_end_of_stream(self, event):
        self.__on_freeze_unfreeze_clicked()

    def _switch_buttons_to_valid_state(self):
        self._close_button.style.button_color = '#00FF00'
        if self._reset_button is not None:
            self._reset_button.style.button_color = '#00FF00'
        self._freeze_unfreeze_button.style.button_color = '#00FF00'

    def _switch_buttons_to_invalid_state(self):
        self._close_button.style.button_color = '#FF0000'
        self._reset_button.style.button_color = '#FF0000'
        self._freeze_unfreeze_button.style.button_color = '#FF0000'

    def _show_error(self, err_desc):
        if self._error_area is None:
            return
        err = "Oops, the following error occurred:\n" + err_desc
        self._error_area.value = err
        self._error_area.rows = 3
        self._error_area.disabled = False
        self._error_area.visibility = 'visible'
        
    def _hide_error(self):
        if self._error_area is None:
            return
        self._error_area.value = ""
        self._error_area.rows = 1
        self._error_area.disabled = True
        self._error_area.visibility = 'hidden'

    @property
    def data_streamer(self):
        return self._data_streamer

    @data_streamer.setter
    def data_streamer(self, ds):
        # print("DataStreamerController.data_streamer.setter.{}".format(self.name))
        # check input parameter
        assert (isinstance(ds, DataStreamer))
        # data streamer
        self._data_streamer = ds
        # route data streamer output to current cell
        self._data_streamer.output = self._ds_output
        # register event handler
        events = [DataStreamEvent.Type.ERROR, DataStreamEvent.Type.RECOVER, DataStreamEvent.Type.EOS]
        self._data_streamer.register_event_handler(self, events)
